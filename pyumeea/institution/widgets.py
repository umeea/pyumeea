from .base import *
import ipywidgets as widgets
from IPython.core.display import  HTML

def eeaWidget(credits=None,mode='widget'):

    table = eeaWidgetHTML(credits)

    mode = mode.lower()
    if mode=='widget':
        table = widgets.HTML(value=table,layout=widgets.Layout(width='100%'))
    elif mode=='html':
        table = HTML(table)

    return table
