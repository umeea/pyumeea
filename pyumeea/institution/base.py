from  ..utils import encodeFile


def logo(fileName,height):
    import os
    fullPathName = os.path.join(os.path.dirname(__file__),'data',fileName)
    svg =   encodeFile(fullPathName)
    height= str(height)+"px"
    return '<img alt="'+fileName+'" style="height:'+height+' !important" src="data:image/svg+xml;base64,'+svg+'" />'

def logo_Qt(fileName,width=0,height=0):
    import os
    fullPathName = os.path.join(os.path.dirname(__file__),'data',fileName)
    png =   encodeFile(fullPathName)
    widthstr=''
    heightstr=''
    if width != 0:
        widthstr='width="{0} "'.format(width)
    if height != 0:
        heightstr='height="{0} "'.format(width)
    return '<img alt="'+fileName+'" {0}{1}src="data:image/png;base64,{2}" />'.format(str(heightstr),str(widthstr),png)


def eeaWidgetHTML(credits=None):

    logoEEA = logo('logoEEA.svg',65)
    logoUM = logo('logoUM.svg',70)
    logoFDS = logo('logoFDS.svg',70)
    creditsLine =  ""
    if credits is not None:
        creditsLine = """<i style='font-size:small'>{0}</i></br>""".format(credits)


    table = "<hr width='100%'>"+creditsLine+ """<table border='0' width='100%' style='margin: 0px;padding:0px;pointer-events: none;'>
        <tr>
            <td style='text-align:left !important'>{0}</td>
            <td style='width:{3}px;padding-right:10px'>{1}</td>
            <td style='width:{4}px'>{2}</td>
        </tr>
    </table>""".format(logoEEA,logoUM,logoFDS,70,70)

    return table


def eeaWidgetHTML_Qt(credits=None):

    logoEEA = logo_Qt('logoEEA-50px.png')
    logoUM = logo_Qt('logoUM-50px.png')
    logoFDS = logo_Qt('logoFDS-50px.png')
    creditsLine =  ""
    if credits is not None:
        creditsLine = "<td width='99%' style='text-align:left;' ><p style='margin-left:15px;font-size:small'>{0}</p></td>".format(credits)

    table = "<hr width='100%'>"+"""<table border='0' width='100%' style='margin: 0px;padding:0px;'>
        <tr>
            <td style='text-align:left !important;white-space: nowrap; padding-right:15px;'>{0}</td>
            <td style='white-space: nowrap;padding-right:15px;'>{1}</td>
            <td style='text-align:right !important:white-space: nowrap;padding-right:15px;'>{2}</td>
            {3}
       </tr>
    </table>""".format(logoEEA,logoUM,logoFDS,creditsLine)
    #return logoEEA
    return table
