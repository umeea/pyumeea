import pprint as pp
import base64
import os

moduleFullPath= os.path.dirname(os.path.abspath(__file__))

def setModulePath(*args):
    moduleFullPath = os.path.join(*args)

def modulePath(*args):
    return os.path.join(moduleFullPath,*args)


### b64  image loader
def encodeFile(*args):
    SVGFileName = modulePath(*args)
    f=open(SVGFileName,'rb')
    svgdata = f.read()
    encoded = base64.b64encode(svgdata).decode("utf-8")
    f.close()
    return encoded

def buildMarkdownTable(inArray,firstLineIsTitle=True):
    theStr=r""
    # convert to markdown
    for idx,row in enumerate(inArray):
        for val in row:
            theStr = theStr + "|"+ val
        theStr = theStr + "|\n"
        if idx==0:
            for val in row:
                theStr = theStr + "|:-:"
            theStr = theStr + "|\n"
    return theStr


def plotMode(mode=None):
    """plotMode.

    La commande :
    plotMode('afficher')
    met Matplotlib en mode affichage : permet les interactions avec les figures (zoom par exemple)
    les figures n'apparaissent pas si on cherche à imprimer le document.

    la commande :
    plotMode('svg')
    met Matplotlib en mode svg, en générant des images en SVG.
    Permet ensuite l'impression directe via le navigateur en haute qualité.

    la commande :
    plotMode('docx')
    met Matplotlib en mode imprimer, en générant des images à haute résolution en PNG.
    Permet ensuite l'exportation via File->Export Notebook As ->Docx
    en conservant les images

    la commande :
    plotMode('latex')
    met matplotlib en mode imprimer pour LaTeX, en générant des images en PDF.
    Permet ensuite l'exportation via File->Export Notebook As ->LaTeX ou -> PDF
    avec la meilleure qualité possible.
    """
    from IPython.display import set_matplotlib_formats
    from IPython import get_ipython

    import matplotlib as mpl

    if mode is None:
        print("Mode défini automatiquement à 'afficher'.")
        mode='afficher'

    goodMode = mode.lower()

    if goodMode=='afficher' or goodMode=='ecran' or goodMode=='screen' or goodMode=='display':
        get_ipython().run_line_magic("matplotlib", "ipympl")
        mpl.rcParams['figure.dpi'] = 110
    elif goodMode=='docx' or goodMode=='word' or goodMode=='office' or goodMode=='bitmap' or goodMode=='png':
        get_ipython().run_line_magic("matplotlib", "inline")
        mpl.rcParams['figure.dpi'] = 300
        set_matplotlib_formats( 'png')
    elif goodMode=='latex' or goodMode=='pdf':
        get_ipython().run_line_magic("matplotlib", "inline")
        mpl.rcParams['figure.dpi'] = 300
        set_matplotlib_formats( 'pdf')
    elif goodMode=='svg':
        get_ipython().run_line_magic("matplotlib", "inline")
        mpl.rcParams['figure.dpi'] = 300
        set_matplotlib_formats( 'svg')
    else:
        raise ValueError("Mode incorrect ! Modes possibles : 'ecran' ou 'word' ou 'latex'.")



def newPlot(totalValues = None):
    import matplotlib.pyplot as plt
    from ipywidgets import FloatProgress
    if totalValues is not None:
        progressBar = FloatProgress(min=0, max=totalValues-1)
        display(progressBar)
    fig1 = plt.figure(figsize=(8,5))#,dpi=150)
    ax1 = fig1.subplots()
    return {'fig':fig1,'ax':ax1,'progressBar':progressBar}

def replot(inPlot,inData,step=None):
    data = inData
    if not isinstance(inData[0],(list,tuple)):
        data = (data,)
    if not isinstance(inData[0][0],(list,tuple)):
        data = (data,)

    inPlot['ax'].clear()
    for toPlot in data:
        #print('ICI')
        #pp.pprint(toPlot)
        if len(toPlot) == 2:
            _args = toPlot[0]
            _kwargs = toPlot[1]
        else:
            _args = toPlot
            _kwargs ={}
        inPlot['ax'].plot(*_args,**_kwargs)
    inPlot['ax'].legend()
    inPlot['fig'].canvas.draw()
    #plt.show(inPlot['fig'])
    if step is  not None or inPlot['progressBar'] is not None:
        inPlot['progressBar'].value = step


import os
import pickle
from datetime import datetime
from random import random

class dataSaver:
    def __init__(self,prefix,*arg):
        self.prefix = prefix
        self.index = 0
        self.folder = os.path.join(*arg)
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)
        self.lastFileName = None
    def generateFileName(self):
        postfix=datetime.now().strftime("%Y-%m-%d--%H-%M-%S-%f-")+"%06d"%int(random()*1e6)+".p"
        fileName =  os.path.join(self.folder,self.prefix + "-" + postfix)
        self.lastFileName = fileName
        return fileName
    def save(self,toSave):
        fileName = self.generateFileName()
        pickle.dump( toSave, open( fileName, "wb" ) )
        return fileName
