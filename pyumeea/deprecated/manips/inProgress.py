import pyvisa
from pymeasure.instruments.tektronix.genericScope import *
from os import path

def findTektro():
    """Recherche de tous les oscilloscopes Tektronix supportés sur les
    interfaces USB et GPIB."""
    rm = pyvisa.ResourceManager()
    devList = rm.list_resources()

    knownDevList = {'TEKTRONIX,TDS 1012,':genericScope,
                'TEKTRONIX,TDS 1012A,':genericScope,
                'TEKTRONIX,TDS 1012B,':genericScope,
                'TEKTRONIX,TDS 1012C,':genericScope,
                'TEKTRONIX,TDS 1012C-EDU,':genericScope,
               }

    driverList = []
    visaAddrList = []


    for dev in devList:
        inst = rm.open_resource(dev)
        instName = inst.query("*IDN?")
        for knownDevName,devDriver in knownDevList.items():
            if instName[0:len(knownDevName)] == knownDevName:
                driverList.append(devDriver)
                visaAddrList.append(dev)
        inst.close()
        del inst

    rm.close()
    del rm

    return (visaAddrList,driverList)

def tektroToCSV(channel,filename,visaAddr,driver=genericScope):
    """Acquisition avec oscilloscope Tektronix.
    "driver" est le nom de l'objet pymeasure qui sert de driver
    "visaAddr" est l'adresse VISA de l'appareil
    La voie "channel" est CH1, CH2 ou MATH.
    Le fichier "filename" sera au format CSV.
    """
    scope = driver(visaAddr)

    x,y,xunit,yunit=scope.waveform.download(channel,True)
    toStore = np.column_stack((x,y))

    np.savetxt(filename, toStore,  delimiter=',',
                  header='X(%s),Y_%s(%s)'%(xunit,channel,yunit))

    #scope.close()
    del(scope)

from IPython.core.display import display, HTML,Latex,Markdown,clear_output
from ipywidgets import Button, HBox, VBox,Layout,HTML,Text,Label,Output,Dropdown,Tab,ToggleButtons
from pylabo.instruments.utils import *

def tektro():
    results=findTektro()

    if results is not None:
        addrList = results[0]
        driverList = results[1]


    display(HTML('''<style> .widget-label { min-width: 20ex !important; }  </style>'''))
    box_layout = Layout(border='solid 1px',padding='10px')
    items_layout = Layout(width='100%',margin='5px auto',padding='0px 10px 0px 10px')

    startWidget = Button( description="Lancer l'acquisition", button_style='success',layout = items_layout)
    fileNameWidget = Text(placeholder='Nom_Fichier',description="Nom du fichier : ",layout = items_layout)
    outputWidget = Output(layout = items_layout);
    channelWidget = Dropdown(options={'Voie 1': 'CH1', 'Voie 2': 'CH2', 'FFT': 'MATH'},value='CH1',description='Signal à acquerir : ',layout = items_layout)
    if len(addrList) == 0:
        scopeListWidget=Label(value="Aucun oscilloscope trouvé.",layout = items_layout)
        startWidget.disabled = True
    else:
        scopeListWidget = Dropdown(options=[(addrList[k],k) for k in range(len(addrList))],value=0,description="Addresse VISA : ",layout=items_layout)
        startWidget.disabled = False

    buttonFileAbort = Button(description='Abandon',button_style='primary')
    buttonFileWrite = Button(description='Acquisition',button_style='warning')
    left_box = VBox([HTML("<p style='line-height:normal;margin-bottom:5px'><b>Ce fichier existe déjà.</b></p><p style='line-height:normal'> Voulez-vous faire l'acquisition et le remplacer  ou abandonner l'acquisition ?</p>")],layout = Layout(width='70%'))
    right_box = VBox([buttonFileAbort,buttonFileWrite ])


    theGUI= VBox(layout = box_layout)
    Line1 = HBox([scopeListWidget,],layout = items_layout)
    Line2 = HBox([channelWidget,fileNameWidget,],layout = items_layout)
    Line3 = HBox([Label(),startWidget,Label()],layout = items_layout)
    Line4 = HBox([left_box, right_box],layout=Layout(border='solid 2px',padding='10px',width='70%',margin='15px auto'))

    Line5 = HBox([outputWidget],layout = items_layout)

    theGUI.children = [Line1,Line2,Label(),Line3,Line4,Line5]
    fileNameWidget.value = 'fichier.CSV'
    display(theGUI)
    Line4.layout.display = 'none'
    with outputWidget:
        display(Markdown('*En attente.*'))


    def doSave():
        tektroToCSV(channelWidget.value,fileNameWidget.value,addrList[scopeListWidget.value],driverList[scopeListWidget.value])
        with outputWidget:
            clear_output()
            chList = channelWidget.options
            display(Markdown('*Dernière Acquisition sur %s // %s // sauvegardé dans "%s"*'%(addrList[scopeListWidget.value],
            list(chList)[list(chList.values()).index(channelWidget.value)],fileNameWidget.value)))

    def on_start_button(b):
        if not path.exists(fileNameWidget.value):
            Line4.layout.display = 'none'
            doSave()
        else:
            Line4.layout.display = 'flex'
            startWidget.disabled = True

    def on_save_abort_button(b):
        Line4.layout.display = 'none'
        startWidget.disabled = False

    def on_do_save_button(b):
        doSave()
        Line4.layout.display = 'none'
        startWidget.disabled = False

    startWidget.on_click(on_start_button)
    buttonFileAbort.on_click(on_save_abort_button)
    buttonFileWrite.on_click(on_do_save_button)


import plotly.graph_objects as go
import time
from threading import Thread

class scope():
    def __init__(self):
        results=findTektro()

        if results is not None:
            self.addrList = results[0]
            self.driverList = results[1]
            self.L={}
            self.W={}
            self.Lines = {}
        self.currentScope = None
        self.refreshThread = Thread(target=self.refreshThreadFunction)
        self.continueThread = True
        self.emergencyStop = False


    def __del__(self):
        self.continueThread = False
        self.refreshThread.stop()

    def mainGUI(self):
        self.buildStyles()
        self.Lines['header'] = self.headerGUI()
        self.Lines['tab'] =self.tabGUI()
        self.Lines['localDialog'] = self.fileBox()
        self.Lines['footer'] = self.footerGUI()
        self.mainGUI= VBox(layout = self.L['box'])
        self.mainGUI.children = [self.Lines['header'],self.Lines['tab'],self.Lines['localDialog'],self.Lines['footer'] ]
        self.on_change_scope()
        display(self.mainGUI)
        self.refreshThread.start()


    def buildStyles(self):
        display(HTML('''<style> .widget-label { min-width: 20ex !important; }  </style>'''))
        self.L['box'] = Layout(border='solid 1px',padding='10px')
        self.L['item'] = Layout(width='100%',margin='5px auto',padding='0px 10px 0px 10px')

    def headerGUI(self):
        if len(self.addrList) == 0:
            self.W['scopeList']=Label(value="Aucun oscilloscope trouvé.",layout = self.L['item'])
        else:
            self.W['scopeList'] = Dropdown(options=[(self.addrList[k],k) for k in range(len(self.addrList))],value=0,description="Addresse VISA : ",layout=self.L['item'])
            self.W['scopeList'].observe(self.on_change_scope)

        return HBox([Label("Oscilloscope : "),self.W['scopeList'],],layout = self.L['item'])

    def footerGUI(self):
        self.W['log']  = Output(layout = self.L['item']);
        with self.W['log']:
            display(Markdown('*En attente.*'))
        return   HBox([self.W['log']],layout = self.L['item'])

    def fileBox(self):
        self.W['fileAbort'] = Button(description='Abandon',button_style='primary')
        self.W['fileWrite'] = Button(description='Acquisition',button_style='warning')
        left_box = VBox([HTML("<p style='line-height:normal;margin-bottom:5px'><b>Ce fichier existe déjà.</b></p><p style='line-height:normal'> Voulez-vous faire l'acquisition et le remplacer  ou abandonner l'acquisition ?</p>")],layout = Layout(width='70%'))
        right_box = VBox([self.W['fileAbort'],self.W['fileWrite'] ])
        self.W['fileAbort'].on_click(self.on_save_abort_button)
        self.W['fileWrite'].on_click(self.on_do_save_button)
        theFileBox = HBox([left_box, right_box],layout=Layout(border='solid 2px',padding='10px',width='70%',margin='15px auto'))
        theFileBox.layout.display = 'none'
        return theFileBox

    def tabGUI(self):
        tab_title = ['Simple', 'Oscilloscope', 'FFTScope']
        children = [self.simpleTab(),self.oscilloscopeTab(),self.FFTScopeTab()]
        tab = Tab()
        tab.observe(self.on_main_tab)
        tab.children = children
        for i in range(len(children)):
            tab.set_title(i, tab_title[i])
        self.W['mainTab'] = tab
        return tab

    def simpleTab(self):
        self.W['simpleStart'] = Button( description="Recupérer les données", button_style='success',layout = self.L['item'])
        self.W['simpleFile'] = Text(placeholder='Nom_Fichier',description="Nom du fichier : ",layout = self.L['item'])
        self.W['simpleChannel'] = Dropdown(options={'Voie 1': 'CH1', 'Voie 2': 'CH2', 'FFT': 'MATH'},value='CH1',description='Signal à acquerir : ',layout = self.L['item'])

        if len(self.addrList) == 0:
            self.W['simpleStart'].disabled = True
        else:
            self.W['simpleStart'].disabled = False

        self.W['simpleFile'].value = 'fichier.CSV'

        L0 = HBox([self.W['simpleChannel'],self.W['simpleFile'],],layout = self.L['item'])
        L1 = Label()
        L2 = HBox([Label(),self.W['simpleStart'],Label()],layout = self.L['item'])

        self.W['simpleStart'].on_click(self.on_start_button)

        return VBox([L0,L1,L2])

    def oscilloscopeTab(self):
        fig = go.FigureWidget( data=[go.Scatter(x=[], y=[])]  )
        self.W['figScope'] = fig
        self.W['oscPlayPause'] = ToggleButtons(options=['Lancer  ▶ ', 'Pause  ▌▌'],
                                description='Acquisition :',disabled=False,button_style='info',value='Pause  ▌▌' )
        self.W['oscPlayPause'].observe(self.on_osc_playPause)
        return  VBox([fig,self.W['oscPlayPause']])

    def FFTScopeTab(self):
        fig = go.FigureWidget()
        self.W['figFFT'] = fig
        return fig

    def doSave(self):
        ch = self.W['simpleChannel'].value
        file = self.W['simpleFile'].value
        x,y,xunit,yunit=self.currentScope.waveform.download(ch,True)
        toStore = np.column_stack((x,y))
        np.savetxt(file, toStore,  delimiter=',',
                  header='X(%s),Y_%s(%s)'%(xunit,ch,yunit))
        with self.W['log']:
            clear_output()
            chList = self.W['simpleChannel'].options
            display(Markdown('*Dernière Acquisition sur %s // %s // sauvegardé dans "%s"*'%(self.addrList[self.W['scopeList'].value],
            list(chList)[list(chList.values()).index(self.W['simpleChannel'].value)],self.W['simpleFile'].value)))

    def on_main_tab(self,evt):
        if 'log'  in self.W.keys():
            with self.W['log']:
                if evt['name']=='selected_index':
                    pass
                    #if evt['new'] == 0: # simple acquisition mode
                    #    self.currentScope.acquisition.sequencingMode='run-stop'
                    #    self.currentScope.acquisition.acquireMode='run'
                    #elif evt['new'] == 1 or evt['new'] == 2: # oscilloscope mode
                    #    self.currentScope.acquisition.sequencingMode='single'
                    #    self.currentScope.acquisition.acquireMode='stop'



                        #print(self.currentScope.isBusy())

                    #print('new',evt['new'])
                    #print('old',evt['old'])

    def on_change_scope(self):
        if len(self.driverList) is not 0:
            del self.currentScope
            scopeNum = self.W['scopeList'].value
            self.currentScope = self.driverList[scopeNum](self.addrList[scopeNum])

    def on_start_button(self,b):
        if not path.exists(self.W['simpleFile'].value):
            self.Lines['localDialog'].layout.display = 'none'
            self.doSave()
        else:
            self.Lines['localDialog'].layout.display = 'flex'
            self.W['simpleStart'].disabled = True

    def on_save_abort_button(self,b):
        self.Lines['localDialog'].layout.display = 'none'
        self.W['simpleStart'].disabled = False

    def on_do_save_button(self,b):
        self.doSave()
        self.Lines['localDialog'].layout.display = 'none'
        self.W['simpleStart'].disabled = False

    def on_osc_playPause(self,b):
        pass
#        self.currentScope.sequencingMode='single'

        #if self.W['oscPlayPause'].value is not 'Lancer  ▶ ' :
            #self.currentScope.acquireMode='stop'
            #self.emergencyStop = False
        #else:
            #self.emergencyStop = True


    def refreshThreadFunction(self):
        while self.continueThread == True:
            handled = False
            if self.W['mainTab'].selected_index == 0:
                pass
            elif self.W['mainTab'].selected_index == 1:
                if self.W['oscPlayPause'].value == 'Lancer  ▶ ' :
                    handled = True
                    self.currentScope.acquisition.acquireStatus='stop'
                    self.currentScope.acquisition.acquireMode='sample'
                    self.currentScope.acquisition.sequencingMode='single'
                    self.currentScope.trigger.mode='auto'                    
                    self.currentScope.acquisition.acquireStatus='run'
                    time.sleep(0.1)
                    print('BUSY:',self.currentScope.isBusy())
                    time.sleep(0.1)
                    print('BUSY:',self.currentScope.isBusy())
                    time.sleep(0.1)
                    print('BUSY:',self.currentScope.isBusy())
                    time.sleep(0.1)
                    print('BUSY:',self.currentScope.isBusy())
                    time.sleep(0.1)
                    print('BUSY:',self.currentScope.isBusy())

                    #while self.currentScope.isBusy() ==1 and self.emergencyStop == False:
                    #    self.currentScope.isBusy()

                    #ch = self.W['simpleChannel'].value
                    #x,y,xunit,yunit=self.currentScope.waveform.download(ch,True)
                    #self.W['figScope'].data[0].x=x
                    #self.W['figScope'].data[0].y=y

            elif self.W['mainTab'].selected_index == 2:
                pass

            if handled==False:
                time.sleep(1)


            #with self.W['log']:
            #    print(self.W['mainTab'].selected_index)
