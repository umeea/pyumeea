## scope Documentation

### Description  
`scope()` est une fonction Python qui permet de réaliser l'acquisition des traces sur la plupart des oscilloscopes USB et GPIB du département EEA, sous la forme d'une interface graphique simple. Il détecte automatiquement les oscilloscopes reconnus qui connectés et visibles à travers la bibliothèque *VISA* de *National Instruments*. Les données sont exportées dans un fichier au format CSV, dont la première colonne est le temps (resp. fréquence), et la deuxième colonne est la tension (resp. la transformée de Fourier de la tension).

### Charger et tracer un fichier CSV  
Voici un code d'exemple qui permet de lire et tracer un fichier CSV :
```python
import matplotlib.pyplot as plt
import numpy as np
t = np.loadtxt('fichier.CSV',delimiter=',',usecols=0)
V1 =  np.loadtxt('fichier.CSV',delimiter=',',usecols=1)
plt.close()
plt.figure()
plt.plot(t,V1)
```

### Liste des oscilloscopes reconnus
Les voici :

<!--import id="displayScopesList"  /-->


### Résolution de problèmes
#### Oscilloscope Tektronix non reconnu
Si l'oscilloscope n'est pas reconnu par le logiciel alors qu'il y est connecté, il faut vérifier que la configuration de connexion n'a pas changé. Il faut aller dans le menu "Utilitaires" puis "Options" et faire en sorte que "Ordinateur" soit sélectionné, comme cela est montré ci-dessous.
<!--img alt="ConfigurerTektro" width="1000" src="manips/data/scopeTektro.png" /-->



<!--import id="bottomBanner"  /-->
