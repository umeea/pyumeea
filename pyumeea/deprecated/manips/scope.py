#
# This file is part of the PyEEA package.
#
# Copyright (c) 2013-2020 PyEEA Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


import pyvisa
from ..instruments.visa.tektronix.scopes import *
from ..instruments.visa.agilent.scopes import *
from ..institution.widgets import *
import sys
from os import path


class scopesHandler:
    """Classe pour gérer les oscilloscopes connectés sous forme d'une interface homogène"""
    knownDevList = {'TEKTRONIX,TDS 1012,':TDS1012,
                'TEKTRONIX,TDS 1012A,':TDS1012,
                'TEKTRONIX,TDS 1012B,':TDS1012,
                'TEKTRONIX,TDS 1012C,':TDS1012,
                'TEKTRONIX,TDS 1012C-EDU,':TDS1012,
                'TEKTRONIX,TBS 1202B,':TBS1202,
                'AGILENT TECHNOLOGIES,DSO-X 2002A,':DSOX2002
                'AGILENT TECHNOLOGIES,DSO1002A,':DSOX2002
               }

    def displayScopesList():
        from ..docGenerator import buildMarkdownTable
        theArray=[]
        theArray.append(["En-tête de la réponse à \*IDN?"])
        for key,stp in scopesHandler.knownDevList.items():
            currentRow =[key,]
            theArray.append(currentRow)
        return buildMarkdownTable(theArray)

    def __init__(self):
        self.scopeList = self.find()
        self.currentScope = None
        self.currentScopeAddress = None
        self.currentScopeShortIDN = None

    def openCurrentScope(self,inAddr=None):
        if inAddr is None:
            if len(self.scopeList) is 0:
                raise ValueError('Aucun oscilloscope connecté ou reconnu !')
            inAddr = list(scopeList.keys())[0]
        if inAddr not in  self.scopeList.keys():
            raise ValueError('Oscilloscope introuvable ou non reconnu !')
        driver = self.scopeList[inAddr][0]
        self.delCurrentScope()
        self.currentScope = driver(inAddr)
        self.currentScopeAddress = inAddr
        self.currentScopeShortIDN = self.scopeList[inAddr][1]


    def delCurrentScope(self):
        del self.currentScope
        self.currentScope = None
        self.currentScopeAddress = None
        self.currentScopeShortIDN = None


    def find(self):
        """Recherche de tous les oscilloscopes supportés sur les
        interfaces USB et GPIB."""
        rm = pyvisa.ResourceManager()
        devList = rm.list_resources()

        driverList = []
        visaAddrList = []

        for dev in devList:
            inst = rm.open_resource(dev)
            instName = inst.query("*IDN?")
            for knownDevName,devDriver in scopesHandler.knownDevList.items():
                if instName[0:len(knownDevName)] == knownDevName:
                    driverList.append((devDriver,knownDevName))
                    visaAddrList.append(dev)
            inst.close()
            del inst
        rm.close()
        del rm
        return dict(zip(visaAddrList,driverList))

    def getData(self,channel):
        """Acquisition avec un oscilloscope reconnu.
        La voie "channel" est channel1, channel2 ou math.
        Modèles reconnus : Tektronix TDS/TPS/TBS, Agilent DSO.
        Renvoie la trace sous forme de (x,y) en array numpy
        """
        x,y,*_=self.currentScope.waveform.download(channel)

        return (x,y)


    def getDataToCSV(self,channel,filename):
        """Acquisition avec un oscilloscope reconnu.
        "driver" est le nom de l'objet pymeasure qui sert de driver
        "visaAddr" est l'adresse VISA de l'appareil
        La voie "channel" est channel1, channel2 ou math.
        Le fichier "filename" sera au format CSV.
        Modèles reconnus : Tektronix TDS/TPS/TBS, Agilent DSO.
        """
        import numpy as np

        x,y= self.getData(channel)
        toStore = np.column_stack((x,y))

        np.savetxt(filename, toStore,  delimiter=',',
                      header='X,Y_%s'%(channel))


def scope():
    """Logiciel de gestion des oscilloscopes.
Lancez scopeDoc() pour plus d'informations.    """
    from IPython.core.display import display, HTML,Latex,Markdown,clear_output
    from ipywidgets import Button, HBox, VBox,Layout,HTML,Text,Label,Output,Dropdown,Tab,ToggleButtons
    #from pylabo.instruments.utils import *
    #results=findScopes()
    scopeHdl=scopesHandler()

    addrList = list(scopeHdl.scopeList.keys())
    dataList = list(scopeHdl.scopeList.values())
    driverList = [item[0] for item in dataList ]
    namesList = [item[1] for item in dataList ]

    display(HTML('''<style> .widget-label { min-width: 20ex !important; }  </style>'''))
    box_layout = Layout(border='solid 1px',padding='10px')
    items_layout = Layout(width='100%',margin='5px auto',padding='0px 10px 0px 10px')

    startWidget = Button( description="Lancer l'acquisition", button_style='success',layout = items_layout)
    fileNameWidget = Text(placeholder='Nom_Fichier',description="Nom du fichier : ",layout = items_layout)
    outputWidget = Output(layout = items_layout);
    channelWidget = Dropdown(options={'Voie 1': 'channel1', 'Voie 2': 'channel2', 'FFT/MATH': 'math'},value='channel1',description='Signal à acquerir : ',layout = items_layout)
    if len(addrList) == 0:
        scopeListWidget=Label(value="Aucun oscilloscope trouvé.",layout = items_layout)
        startWidget.disabled = True
    else:
        scopeListWidget = Dropdown(options=[(namesList[k]+' [ '+addrList[k]+' ]',k) for k in range(len(addrList))],value=0,description="Addresse VISA : ",layout=items_layout)
        startWidget.disabled = False

    buttonFileAbort = Button(description='Abandon',button_style='primary')
    buttonFileWrite = Button(description='Acquisition',button_style='warning')
    left_box = VBox([HTML("<p style='line-height:normal;margin-bottom:5px'><b>Ce fichier existe déjà.</b></p><p style='line-height:normal'> Voulez-vous faire l'acquisition et le remplacer  ou abandonner l'acquisition ?</p>")],layout = Layout(width='70%'))
    right_box = VBox([buttonFileAbort,buttonFileWrite ])


    theGUI= VBox(layout = box_layout)
    Line1 = HBox([scopeListWidget,],layout = items_layout)
    Line2 = HBox([channelWidget,fileNameWidget,],layout = items_layout)
    Line3 = HBox([Label(),startWidget,Label()],layout = items_layout)
    Line4 = HBox([left_box, right_box],layout=Layout(border='solid 2px',padding='10px',width='70%',margin='15px auto'))

    Line5 = HBox([outputWidget],layout = items_layout)
    Line6 = HBox([eeaWidget('v 1.0 - par M. Myara et S. Blin - Département EEA / IES UMR5214 - Université de Montpellier')])

    theGUI.children = [Line1,Line2,Label(),Line3,Line4,Line5,Line6]
    fileNameWidget.value = 'fichier.CSV'
    display(theGUI)
    Line4.layout.display = 'none'
    with outputWidget:
        display(Markdown('*En attente.*'))


    def doSave():
        with outputWidget:
            try:
                scopeHdl.openCurrentScope(addrList[scopeListWidget.value])
                scopeHdl.getDataToCSV(channelWidget.value,fileNameWidget.value)
                clear_output()
                chList = channelWidget.options
                display(Markdown('*Dernière Acquisition sur* **%s** *[%s] //* **%s** *// sauvegardé dans* **"%s"**'%(scopeHdl.currentScope.name,addrList[scopeListWidget.value],
                list(chList)[list(chList.values()).index(channelWidget.value)],fileNameWidget.value)))
            except pyvisa.VisaIOError:
                clear_output()
                display(Markdown("**Erreur - problème de communication avec l'oscilloscope.**"))#sys.exc_info()[0]))
            except IOError:
                clear_output()
                display(Markdown("**Erreur - problème avec fichier.**"))#sys.exc_info()[0]))


    def on_start_button(b):
        if not path.exists(fileNameWidget.value):
            Line4.layout.display = 'none'
            doSave()
        else:
            Line4.layout.display = 'flex'
            startWidget.disabled = True

    def on_save_abort_button(b):
        Line4.layout.display = 'none'
        startWidget.disabled = False

    def on_do_save_button(b):
        doSave()
        Line4.layout.display = 'none'
        startWidget.disabled = False

    startWidget.on_click(on_start_button)
    buttonFileAbort.on_click(on_save_abort_button)
    buttonFileWrite.on_click(on_do_save_button)



def scopeDoc():
    from ..docGenerator import parseMarkdownFile
    import os.path
    from IPython.core.display import display, HTML,Markdown
    from ..institution.widgets import eeaWidget


    docFile = 'data/scopeDoc.md'
    moduleFullPath= os.path.dirname(os.path.abspath(__file__))
    MDFileName = os.path.join(moduleFullPath,docFile)
    credits = 'v 1.0 - par M. Myara et S. Blin - Département EEA / IES UMR5214 - Université de Montpellier'
    text= parseMarkdownFile(MDFileName,imports={'displayScopesList':scopesHandler.displayScopesList(),\
                                                            'bottomBanner':eeaWidget(credits=credits,mode='htmlCode')} )
    display(Markdown(text))
