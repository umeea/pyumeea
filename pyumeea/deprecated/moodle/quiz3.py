#
# This file is part of the PyEea package.
#
# Copyright (c) 2013-2020 PyMeasure Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import lxml.etree as ET
import lxml
import markdown2 as md
import re
import base64
from bs4 import BeautifulSoup,NavigableString
from os.path import basename, splitext
import random


### b64  image loader


class quizEncoder:    
                
    class answersClass(list):
        def __init__(self):
            pass

        def __iadd__(self,other):
            self.append(other)
            return self
    
    def __init__(self,name=''):
        self.questions = []
        self.initialCategoryName = name

        
    
    ## basic interface
    def newSubQuestion(self,question,kind,name=None,add=True):
        if question['questionKind'] != 'cloze':
            raise ValueError('Les sous-questions ne sont possibles que pour les questions de type "cloze"')
        
        if name is not None :
            pass
        elif name == None and add==True:
            name = 'Q'+str(len(self.questions))+'.'+str(len(self.questions[-1]['subquestions']))
        else: name = 'Q#x.'+str(random.randint(0,9999))
            
        if kind not in ('multichoice','shortanswer','numerical') :
            raise ValueError('Les sous-questions ne peuvent être que du type "multichoice", "shortanswer" ou "numerical"')
            
        SQ  =  self.newQuestion(kind,name,False)
        if add == True:
            question['subQuestions']+=SQ
        return SQ
    
    def newQuestion(self,kind,name=None,add=True):
        if name is not None:
            pass
        elif name == None and add==True:
            name = 'Q'+str(len(self.questions))
        else: name = 'Q#'+str(random.randint(0,9999))
            
        Q = {}
        Q['questionKind'] = kind
        Q['name']    = name
        Q['text']    = ''
        Q['answers'] = None
        Q['subQuestions'] = None
        
        if kind in('multichoice' , 'shortanswer' , 'numerical','gapselect'): 
            Q['answers'] = self.answersClass() 
        if kind == 'cloze':
            Q['subQuestions'] = self.answersClass()  
        if kind =='essay':pass
        
        if add == True:
            self.addQuestion(Q)
        return Q        
    
    def addQuestion(self,Question):
        self.questions.append(Question)
                         
    ## global engine    
    def encodeSingleQuestion(self,questionData):
        questionTags = self.buildQuestionTags(questionData['questionKind'])
        self.updateTags(questionTags,questionData)
        
        answers = None
        subQuestions = None # cloze mode
        if 'answers' in questionData.keys() and questionData['answers'] is not None:
            answers = self.buildAnswers(questionData)
        if 'subQuestions' in questionData.keys() and questionData['subQuestions'] is not None:
            subQuestions = self.buildSubQuestions(questionData)
            
        toReturn = {}
        toReturn['questionKind'] = questionData['questionKind']
        toReturn['questionTags'] = questionTags
        toReturn['answers'] = answers
        toReturn['subQuestions'] = subQuestions
        return toReturn        
    
    def buildQuestionTags(self,questionKind):
        # specific 'category' tag
        if questionKind == 'category':
            tags={}
            tags['text']={'name':'category','kind':'textTag','values':{'data':'','textFormat':'txt'}}
            return tags

        # general purpose tags
        tags={}
        tags['name']={'name':'name','kind':'textTag','values':{'data':'','textFormat':'txt'}}
        tags['text']={'name':'questiontext','kind':'textTag','values':{'data':'','textFormat':'MD'}}
        tags['defaultgrade']={'name':'defaultgrade','kind':'valueTag','values':{'data':1.0}}
        tags['generalfeedback']={'name':'generalfeedback','kind':'textTag','values':{'data':None,'textFormat':'MD'}}
        tags['penalty']={'name':'penalty','kind':'valueTag','values':{'data':0.1}}
        tags['hidden']={'name':'hidden','kind':'valueTag','values':{'data':0}}
        tags['single']={'name':'single','kind':'booleanTag','values':{'data':False}}
        tags['idnumber']={'name':'idnumber','kind':'valueTag','values':{'data':None}}        
        tags['tags']={'name':'tags','kind':'textTag','values':{'data':None,'textFormat':'txt'}}
        
        # specific tags
        if questionKind == 'multichoice':
            tags['shuffleanswers']={'name':'shuffleanswers','kind':'valueTag','values':{'data':1}}
            tags['answernumbering']={'name':'answernumbering','kind':'valueTag','values':{'data':'abc'}}
            tags['clozeoption']={'name':'clozeoption','kind':'valueTag','values':{'data':None}}
            
        elif questionKind == 'numerical':
            pass            
        
        elif questionKind == 'shortanswer':
            tags['usecase']={'name':'usecase','kind':'valueTag','values':{'data':0}}  
        
        elif questionKind == 'cloze':
            tags['mode']={'name':'mode','kind':'valueTag','values':{'data':'basic'}}        
                        
        elif questionKind == 'essay':
            tags['responseformat']={'name':'responseformat','kind':'valueTag','values':{'data':'editorfilepicker'}}
            tags['responserequired']={'name':'responserequired','kind':'valueTag','values':{'data':'0'}}
            tags['responsefieldlines']={'name':'responsefieldlines','kind':'valueTag','values':{'data':'15'}}
            tags['attachments']={'name':'attachments','kind':'valueTag','values':{'data':'1'}}
            tags['attachmentsrequired']={'name':'attachmentsrequired','kind':'valueTag','values':{'data':'1'}}
            tags['graderinfo']={'name':'graderinfo','kind':'textTag','values':{'data':'','textFormat':'MD'}}
            tags['responsetemplate']={'name':'responsetemplate','kind':'textTag','values':{'data':None,'textFormat':'MD'}}  
            
        elif questionKind == 'description':
            tags.pop('single')
            tags['penalty']['values']['data']=0
            
        elif questionKind == 'gapselect':
            tags['correctfeedback']={'name':'correctfeedback','kind':'textTag','values':{'data':'','textFormat':'MD'}}  
            tags['partiallycorrectfeedback']={'name':'partiallycorrectfeedback','kind':'textTag','values':{'data':'','textFormat':'MD'}}  
            tags['incorrectfeedback']={'name':'incorrectfeedback','kind':'textTag','values':{'data':'','textFormat':'MD'}}  
            tags['hint']={'name':'hint','kind':'textTag','values':{'data':None,'textFormat':'MD'}}  
            

        return tags

            
    def updateTags(self,tags,data):
        for tagName,tagData in data.items():
            if tagName in tags.keys():
                tags[tagName]['values']['data'] = tagData

        # remove data=None tags
        noneTags = []
        for tagName,tagData in tags.items():
            if tagData['values']['data'] == None:
                noneTags.append(tagName)

        for k in noneTags:
            tags.pop(k)                 

    def buildAnswersTagsAndAttributes(self,questionKind):                
        # general answers tags
        tags = {}
        tags['feedback']={'name':'feedback','kind':'textTag','values':{'data':None,'textFormat':'MD'}}
        attributes = {}
        attributes['score']=False
        attributes['textFormat']='MD'                    
        
        # specific answers tags
        if questionKind == 'multichoice':
            attributes['textFormat']='MD'            
        elif questionKind =='shortanswer':
            attributes['textFormat']='plain_text' 
        elif questionKind == 'numerical':
            attributes['textFormat']='plain_text'            
            tags['tolerance']={'name':'tolerance','kind':'valueTag','values':{'data':0}}
        elif questionKind == 'gapselect':
            attributes['textFormat']='txt'  
            tags['group']={'name':'group','kind':'valueTag','values':{'data':1}}
            
        return (tags,attributes)
        
    def buildAnswers(self,questionData):
        (tags,attributes) = self.buildAnswersTagsAndAttributes(questionData['questionKind'])
        data = questionData['answers']
        retAnswers = []
        
        # calculate score : first pass
        scoresList = []
        for answer in data:
            scoresList.append(float(answer['score']) if 'score' in answer else 0)
        totalScore = 0            
        for score in scoresList:
            totalScore += score             
        maxScore=max(scoresList)
                        
        # handle score last pass and other updates
        for answer in data:
            localAttributes = {}
            localAttributes.update(attributes)
            localScore = float(answer['score']) if 'score' in answer else 0
            if questionData['questionKind'] != 'gapselect':
                localAttributes['fraction'] = 100*localScore
            if 'textFormat' in answer.keys():
                localAttributes['textFormat'] = answer['textFormat'] 
            localAttributes.pop('score')            
            localTags = {}
            localTags.update(tags)
            self.updateTags(localTags,answer)
            retAnswers.append({'attributes':localAttributes,'tags':localTags,'text':answer['text']})

            
        # normalize score sometimes
        scoreNorm = totalScore if questionData['questionKind'] == 'multichoice' else 1
        scoreNorm = maxScore if questionData['questionKind'] == 'shortanswer' else 1
        
        for answer in retAnswers:
            attr = answer['attributes']
            if 'fraction' in answer:                
                attr['fraction']/=scoreNorm                                      
        return retAnswers
                             
    def buildSubQuestions(self,questionData):
        retAnswers = []
        
        for subQuestion in questionData['subQuestions']:
            retAnswers.append(self.encodeSingleQuestion(subQuestion))
        
        return retAnswers                    
                             
    ## Moodle XML and HTML encoding related code
    def encodeFileAsBase64(self,filename):
        f=open(filename,'rb')
        fileData = f.read()
        encoded = base64.b64encode(fileData).decode("utf-8")
        f.close()
        return encoded        
        
    def encodeAsMoodleXML(self):
        XML = ET.Element('quiz')      
        cat = self.newQuestion('category',add=False)
        cat['text'] = self.initialCategoryName
        questions = [cat] + self.questions
        for question in questions:
            print(question['questionKind'])
            XMLQuestion = self.singleQuestionToMoodleXMLElement(question)
            XML.append(XMLQuestion)
        return ET.tostring(XML,encoding='unicode', pretty_print=True)    
    
    def singleQuestionToMoodleXMLElement(self,questionData):
        encodedQuestion = self.encodeSingleQuestion(questionData)
        questionTags = encodedQuestion['questionTags']
        answers = encodedQuestion['answers']
        subQuestions = encodedQuestion['subQuestions']            
        
        # create basic question
        question = ET.Element('question',attrib={'type':encodedQuestion['questionKind']})

        # insert tags in question
        for tagName,tagValue in questionTags.items():
            # tags encoding is specific
            if tagValue['name'] == 'tags':
                alltags = tagValue['values']['data'].split(',')
                xmlTag = ET.Element('tags')
                for tag in alltags:
                    tag = tag.strip()
                    localTagValues = {}; localTagValues.update(tagValue['values'])
                    localTagValues['data']=tag
                    insertedTextTag = self.textTag_MoodleXML('tag',**localTagValues) 
                    xmlTag.append(insertedTextTag)
            
            # specific case for cloze environment and sub-questions
            elif encodedQuestion['questionKind'] == 'cloze' and tagValue['name'] == 'questiontext':
                localTagValues = {}; localTagValues.update(tagValue['values'])
                if subQuestions is not None:
                    localTagValues['data'] = localTagValues['data'] +'    \n'+ self.encodeClozeTag(encodedQuestion,subQuestions) 
                    xmlTag = self.textTag_MoodleXML('questiontext',**localTagValues)
            # specific case for category
            elif encodedQuestion['questionKind'] == 'category' and tagValue['name'] == 'category':
                localTagValues = {}; localTagValues.update(tagValue['values'])
                localTagValues['data']='$module$/'+localTagValues['data']
                xmlTag = self.textTag_MoodleXML('category',**localTagValues)
                                
            else:
                func = eval('self.'+tagValue['kind']+'_MoodleXML') 
                xmlTag = func(tagValue['name'],**tagValue['values'])
            question.append(xmlTag)  
            
        if answers is not None:
            for answer in answers:
                cleanedAttr = {}
                cleanedAttr.update(answer['attributes'])
                cleanedAttr.pop('textFormat')
                answerXMLTagName = 'answer'
                if encodedQuestion['questionKind'] == 'gapselect':
                    answerXMLTagName = 'selectoption'
                    
                answTag = self.textTag_MoodleXML(answerXMLTagName,data=answer['text'],\
                                textFormat=answer['attributes']['textFormat'],\
                                attributes=cleanedAttr)
                for tagName,tagValue in answer['tags'].items():
                    func = eval('self.'+tagValue['kind']+'_MoodleXML') 
                    xmlTag = func(tagValue['name'],**tagValue['values'])                    
                    answTag.append(xmlTag)
                
                question.append(answTag)
                        
        return question   
    
    def encodeClozeTag(self,encodedQuestion,subQuestions):
        mode = encodedQuestion['questionTags']['mode']['values']['data']
        endpar = '    \n' if mode == 'basic' else ''    
        
        IDList={}
        subQuestionsText = ''
                
        for defaultID,subQuestion in enumerate(subQuestions):
            grade = subQuestion['questionTags']['defaultgrade']['values']['data']
            feedback = ''
            if 'feedback' in subQuestion['answers'][0]:
                feedback = subQuestion['answers']['feedback']  
            elif 'generalfeedback' in subQuestion['questionTags']:                
                feedback = subQuestion['questionTags']['generalfeedback']
            if len(feedback)==0:
                feedback=None                
            
            value = subQuestion['answers'][0]['text']                        
            theID = subQuestion['questionTags']['idnumber']['values']['data'] if 'idnumber' in subQuestion['questionTags'] else str(defaultID)
            
            subQuestionsText += subQuestion['questionTags']['text']['values']['data'] + endpar
            
            if subQuestion['questionKind'] == 'numerical':
                tolerance = subQuestion['answers'][0]['tags']['tolerance']['values']['data']
                subQuestionClozeCommand = '{%d:NUMERICAL:=%d:%d}' % (float(grade),float(value),float(tolerance))   
            
            if subQuestion['questionKind'] == 'shortanswer' or\
                subQuestion['questionKind'] ==  'multichoice':
                keyword = 'SHORTANSWER' if subQuestion['questionKind']=='shortanswer' else 'MULTICHOICE'
                
                if subQuestion['questionKind'] == 'multichoice' :
                    keywordextra1 = ''; keywordextra2 = ''
                    if 'clozeoption' in subQuestion['questionTags']:
                        clozeoption = subQuestion['questionTags']['clozeoption']['values']['data']
                        keywordextra1 ='V' if clozeoption == 'vertical' else ''
                        keywordextra1 ='H' if clozeoption == 'horizontal' else ''
                    if 'shuffleanswers' in subQuestion['questionTags']:
                        keywordextra2 = 'S' if subQuestion['questionTags']['shuffleanswers']['values']['data'] == 1 else ''
                    if len(keywordextra1) + len(keywordextra1) == 0: pass
                    else:
                        keyword+='_%s%s'%(keywordextra1,keywordextra2)
                    
                subQuestionClozeCommand =  '{%d:%s:'%(float(grade),keyword)
                #string+='{%d:%s:'%(float(grade),keyword)
                answersList = []
                for answer in subQuestion['answers']:
                    if answer['attributes']['fraction'] == 0.0:
                        answersList.append(answer['text'])
                    elif answer['attributes']['fraction'] == 100.0:
                        answersList.append('='+answer['text'])
                    else:
                        answersList.append( '={faction=%d}%s'% (answer['attributes']['fraction'],answer['text']))                                            
                subQuestionClozeCommand +='~'.join(answersList)+'}' 
                
            
            IDList[theID] = subQuestionClozeCommand
            if mode == 'basic': subQuestionsText+=r'\{%s\}'%(theID)  + endpar              
                
        def IDedBlockReplacer(inBloc):
            theID = inBloc.group(2)
            return IDList[theID]

        string = re.sub(r'(\\\{)((?!\\\})[\s\S])*\\\}',IDedBlockReplacer,subQuestionsText) 
        return string

    
    def textTag_MoodleXML(self,tagName,data=None,textFormat='txt',attributes={}):
        localAttributes = {}
        html = None
        theText = data
        
        if textFormat.lower() != 'txt':
            localAttributes['format'] = textFormat.lower()           
        
        if textFormat.lower() == 'md':
            localAttributes['format'] = 'html'
        
        for key,val in attributes.items():
            localAttributes[key] = str(val)
            
            
                    
        mainTag = ET.Element(tagName,attrib = localAttributes)            
        textTag = ET.Element('text')
        

        if data is not None:        
            if textFormat.lower() == 'md':
                html = md.markdown(data,extras = ('tables','fenced-code-blocks','code-friendly'))
            elif textFormat.lower() == 'html':
                html = data
            
            if html is not None:
                html = self.parseHTML(html,MoodleMode=True)
                theText = ET.CDATA(html)
                                   
            textTag.text = theText
                    
        mainTag.append(textTag)
        return mainTag
      
    def valueTag_MoodleXML(self,tagName,data,attributes={}):
        vT = ET.Element(tagName,attrib=attributes)
        vT.text = str(data)
        return vT
    
    def booleanTag_MoodleXML(self,tagName,data,attributes={}):
        bT = ET.Element(tagName,attrib=attributes)
        bT.text = 'true' if data else 'false'
        return bT                                    
                              
    def parseHTML(self,html,MoodleMode=False):
        
        soup = BeautifulSoup(html,"html.parser")#, from_encoding="utf-8")

        for tag in soup.findAll():
            if tag.name =='img':
                tag['src'] = "data:image/jpg;base64,"+self.encodeFileAsBase64(tag['src'])    
            # treat all LaTeX $$ and $ sequences but avoid code sections
            elif tag.name != 'code':
                def mathReplacer(inBloc):
                    theStr = inBloc.group(0)
                    pythonEscapes = {r'\\':r'\\\\',r'\a':r'\\a',r'\b':r'\\b',r'\f':r'\\f',\
                                     r'\n':r'\\n',r'\r':r'\\r',r'\t':r'\\t',\
                                     r'\v':r'\\v',r'\x':r'\\x'}
                    for fromChar,toChar in pythonEscapes.items():
                        theStr.replace(fromChar,toChar)
                    
                    if MoodleMode == True: 
                        theStr.replace(r'\n', r'')
                        if theStr[0:2]=='$$':
                            newString = r'\['+theStr[2:-2]+r'\]'
                            return newString
                        elif theStr[0:1]=='$':
                            newString = r'\('+theStr[1:-1]+r'\)'
                            return newString
                    else:
                        return theStr
                    
                for subtag in tag:
                    if isinstance(subtag,NavigableString):
                        newString = re.sub(r'(\${1,2})(?:(?!\1)[\s\S])*\1',mathReplacer,subtag) 
                        subtag.replace_with(newString)
                             
        return str(soup)      
    
    # does not work in the context of Moodle
    def codeHiliter(self):
        import os
        moduleFullPath= os.path.dirname(os.path.abspath(__file__))
        
        cssFile = 'jupyterlab_codehilite.css'    
        cssFileName = os.path.join(moduleFullPath,'..','data',cssFile)        
        cssData = open(cssFileName,'rb').read().decode('utf-8') +\
        """.jp-RenderedHTMLCommon th {
        font-weight: bold;
        text-align: center !important;}
        .codehilite pre {
        background:var(--jp-rendermime-table-row-background);
        margin-bottom: 10px;
        }    
        .codehilite code {
            background-color: var(--jp-rendermime-table-row-background);

        }
        """

        return '<style>\n'+cssData+'</style>\n'
    def encodeAsMarkdownPreview(self):
        from mdutils.mdutils import MdUtils        
        MDEncoder = MdUtils('Dummy')
        questions = self.questions
        MDEncoder.new_header(level=1, title='Main Title')
        for question in questions:
            print(question['questionKind'])
            self.singleQuestionToMarkdownPreview(question,MDEncoder)
        return MDEncoder.file_data_text          
    
    def singleQuestionToMarkdownPreview(self,questionData,MDEncoder):        
        encodedQuestion = self.encodeSingleQuestion(questionData)
        questionTags = encodedQuestion['questionTags']
        answers = encodedQuestion['answers']
        subQuestions = encodedQuestion['subQuestions']            
        
        # create  question title
        #print(encodedQuestion)
        questionHeader = '<font color="Indigo">('+encodedQuestion['questionKind']+') '+questionTags['name']['values']['data']+"</font>"
        MDEncoder.new_header(level=2, title=questionHeader)
        question = ET.Element('question',attrib={'type':encodedQuestion['questionKind']})

        # insert tags in question
        textList = []
        for tagName,tagValue in questionTags.items():
            if tagName ==  'text':
                pass
            else:
                func = eval('self.'+tagValue['kind']+'_MarkdownPreview') 
                if tagName not in ('name,'):
                    textList.append(func(tagValue['name'],**tagValue['values']))
        MDEncoder.new_paragraph(r'ATTRIBUTES :' + r' -- '.join(textList)+"",'i', color='RebeccaPurple')
        self.displayQuestionText_MarkdownPreview(MDEncoder,encodedQuestion)
        
        self.buildAnswers_MarkdownPreview(MDEncoder,\
                                                encodedQuestion['questionKind'],answers)
        MDEncoder.new_paragraph('\n<hr>')        


    def displayQuestionText_MarkdownPreview(self,MDEncoder,encodedQuestion):
        questionTags = encodedQuestion['questionTags']
        answers = encodedQuestion['answers']
        subQuestions = encodedQuestion['subQuestions']    
        
        questionText = questionTags['text']['values']['data']
        questionFormat = questionTags['text']['values']['textFormat']
        
        if encodedQuestion['questionKind'] == 'gapselect':
            groupsList={}
            questionList = {}
            for num,answer in enumerate(answers):
                group = answer['tags']['group']['values']['data']
                text = answer['text']
                if group not in groupsList.keys():
                    groupsList.update({group:[]})
                questionID = num+1
                groupsList[group].append((questionID,text))
                questionList[questionID] = group
                
            def selectorReplacer(inBloc):
                value = int(inBloc.group(1))
                if value not in questionList.keys():
                    raise ValueError('[[%d]] : Valeur non définie !'%value)
                answersList = groupsList[questionList[value]]
                selectStr = r'<select>'
                for ans in answersList:
                    selected=''         
                    trueFalse = ''
                    if ans[0] == value:
                        selected = 'selected'
                        trueFalse = '&#10004;'
                    selectStr += r'<option value="%s" %s>%s %s</option>'%(str(group)+'.'+str(ans[0]),selected,trueFalse,ans[1])
                selectStr += r'</select>'
                return selectStr
            questionText = self.text_MarkdownPreview(questionText,questionFormat)             
            questionText = re.sub(r'\[\[\s*(\d+)\s*\]\]',selectorReplacer,questionText) 
        
        else:
            questionText = self.text_MarkdownPreview(questionText,questionFormat)         

        MDEncoder.new_paragraph(questionText)        
        
                
    def buildAnswers_MarkdownPreview(self,MDEncoder,questionKind,answers):
        if questionKind == 'multichoice':
            if answers is not None:
                items = []
                for num,answer in enumerate(answers):
                    score = answer['attributes'][ 'fraction']
                    text = answer['text']
                    color = 'red' if score == 0 else 'green'
                    letter = str(chr(b"A"[0]+num))
                    text = "<font color='%s'>"%(color) +letter+" (% 3d) - </font>"%(score)+" "+text+""
                    text = self.text_MarkdownPreview(text, answer['attributes'][ 'textFormat'] )
                    items.append(text)
                MDEncoder.new_list(items=items)
        if questionKind == 'numerical':
            numString = str(answers[0]['text'])+' ± '+str(answers[0]['tags']['tolerance']['values']['data'])
            MDEncoder.new_paragraph("<font color='green'>"+numString+"</font>")
            pass
        if questionKind == 'shortanswer':
            pass
        if questionKind == 'cloze':
            pass
        if questionKind == 'gapselect':
            pass
        if questionKind == 'essay':
            MDEncoder.new_paragraph('''<textarea cols="40" rows="3"></textarea>''')


    def text_MarkdownPreview(self,data,textFormat='txt'):
        html = None
        if data is None:
            data=''
        if textFormat.lower() == 'md':
            html = md.markdown(data,extras = ('tables','fenced-code-blocks','code-friendly'))
        elif textFormat.lower() == 'html':
            html = data

        if html is not None:
            html = self.parseHTML(html,MoodleMode=False)
            data = html
            
        return data
    
    def textTag_MarkdownPreview(self,tagName,data=None,textFormat='txt',attributes={}):
        if data is None:
            data=''
        return tagName+' : '+self.text_MarkdownPreview(data)

    def valueTag_MarkdownPreview(self,tagName,data,attributes={}):
        return tagName+' : '+str(data)

    def booleanTag_MarkdownPreview(self,tagName,data,attributes={}):
        return tagName+' : '+str(data)
    
    
    
    
class quiz:
    def __init__(self,name):
        self.XML = quizEncoder(name)

    def newQuestion(self,kind,name=None,add=True):
        return self.XML.newQuestion(kind,name,add)
    
    def newSubQuestion(self,question,kind,name=None,add=True):
        return self.XML.newSubQuestion(question,kind,name,add)
    
          
    def getMoodleXMLString(self,addHeader = True):
        header = ''
        if addHeader:
            header = '<?xml version="1.0" encoding="UTF-8"?>\n' 
        return header+self.XML.encodeAsMoodleXML()
    
    def saveAsMoodleXML(self,filename):
        open(filename,'w').write(self.getMoodleXMLString())        

