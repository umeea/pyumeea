#
# This file is part of the PyEea package.
#
# Copyright (c) 2013-2020 PyMeasure Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import lxml.etree as ET
import lxml
import markdown2 as md
#import codecs
import re
import base64
from bs4 import BeautifulSoup,NavigableString
#import html


#import bs4.dammit
#import bs4.builder._htmlparser

# when analyzing spam, we always run into situations where the sender tries
# hard to obfuscate their input in order to sneak by spam detecting engines.
# instead of "ABC", they'll use HTML Entity references; &#65;&#66;&#67; in
# order to extract the viewer readable segments, we need a parser.  most
# parsers try to be smart and clean things up.  BeautifulSoup and lxml are
# two common parsers.  lxml is largely compiled C so we can't tweak it very
# easily.

# step #1
# BeautifulSoup insists on always doing entity substitution and there's no
# way to politely tell it to fuck off.  override the hex->int and
# word->symbol conversions, simply append our data to the growing stack
#_handle_data = bs4.builder._htmlparser.BeautifulSoupHTMLParser.handle_data
#bs4.builder._htmlparser.BeautifulSoupHTMLParser.handle_charref   = lambda cls,s: _handle_data(cls, '&#'+s+';')
#bs4.builder._htmlparser.BeautifulSoupHTMLParser.handle_entityref = lambda cls,s: _handle_data(cls, '&'+s+';')

# step #2
# BeautifulSoup insists on further ensuring printed data is always tidy and
# semantically correct, thus it ALWAYS does entity substitution even after
# we refused to do it above.  the below ensures the __str__ methods don't
# attempt to mangle the serialized data.  this simply returns the original
# matched input when the substitution methods are called
#bs4.dammit.EntitySubstitution._substitute_html_entity = lambda o: o.group(0)
#bs4.dammit.EntitySubstitution._substitute_xml_entity  = lambda o: o.group(0)





from os.path import basename, splitext


### b64  image loader
def encodeFile(filename):
    f=open(filename,'rb')
    svgdata = f.read()
    encoded = base64.b64encode(svgdata).decode("utf-8")
    f.close()
    return encoded



class moodleQuizXML:    
    
    def __init__(self):
        self.Q = ET.Element('quiz')
    
    def add(self,inContent):
        self.Q.append(inContent)        

        
    def parseHTML(self,html,replaceLatex=True):
        soup = BeautifulSoup(html,"html.parser")#, from_encoding="utf-8")

        for tag in soup.findAll():
            if tag.name =='img':
                tag['src'] = "data:image/jpg;base64,"+encodeFile(tag['src'])    
            # treat all LaTeX $$ and $ sequences but avoid code sections
            elif tag.name != 'code':
                def mathReplacer(inBloc):
                    theStr = inBloc.group(0).replace('\n', '')
                    if theStr[0:2]=='$$':
                        newString = r'\['+theStr[2:-2]+r'\]'
                        return newString
                    elif theStr[0:1]=='$':
                        newString = r'\('+theStr[1:-1]+r'\)'
                        return newString
                for subtag in tag:
                    if isinstance(subtag,NavigableString):
                        newString = re.sub(r'(\${1,2})(?:(?!\1)[\s\S])*\1',mathReplacer,subtag) 
                        subtag.replace_with(newString)
                             
        return str(soup)           
        
    def textTag(self,tagName,text=None,textFormat='txt',attributes={}):
        localAttributes = {}
        html = None
        theText = text
        
        if textFormat.lower() != 'txt':
            localAttributes['format'] = textFormat.lower()           
        
        if textFormat.lower() == 'md':
            localAttributes['format'] = 'html'
        localAttributes.update(attributes)
                    
        mainTag = ET.Element(tagName,attrib = localAttributes)            
        textTag = ET.Element('text')
        

        if text is not None:        
            if textFormat.lower() == 'md':
                html = md.markdown(text,extras = ('tables','fenced-code-blocks','code-friendly'))
            elif textFormat.lower() == 'html':
                html = text
            
            if html is not None:
                html = self.parseHTML(html,True)
                theText = ET.CDATA(html)
                                   
            textTag.text = theText
                    
        mainTag.append(textTag)
        return mainTag
      
    def valueTag(self,tagName,value,attributes={}):
        vT = ET.Element(tagName,attrib=attributes)
        vT.text = str(value)
        return vT
    
    def booleanTag(self,tagName,value,attributes={}):
        bT = ET.Element(tagName,attrib=attributes)
        bT.text = 'true' if value else 'false'
        return bT
        
    def toString(self):
        return ET.tostring(self.Q,encoding='unicode', pretty_print=True)
        
    def commonTagsList(self):
        tags={}
        tags['name']={'name':'name','kind':'textTag','values':{'text':'','textFormat':'txt'}}
        tags['text']={'name':'questiontext','kind':'textTag','values':{'text':'','textFormat':'MD'}}
        tags['defaultgrade']={'name':'defaultgrade','kind':'valueTag','values':{'value':1.0}}
        tags['generalfeedback']={'name':'generalfeedback','kind':'textTag','values':{'text':None,'textFormat':'MD'}}
        tags['penalty']={'name':'penalty','kind':'valueTag','values':{'value':0.1}}
        tags['hidden']={'name':'hidden','kind':'valueTag','values':{'value':0}}
        tags['single']={'name':'single','kind':'booleanTag','values':{'value':False}}

        return tags    
    
    def addAnswerOptions(self,questionName,answerTag,options):
        
        # default values for required options, depending on question kind
        localOptions = {}
        if questionName == 'multichoice':
            pass
        elif questionName =='shortanswer':
            pass
        elif questionName == 'numerical':
            options['tolerance'] = 0
        
        # override default values fron input options
        for optionName,value in options.items():
            localOptions[optionName]=value
            
        # now insert options in answerTag with the good shape
        tag = None        
        for optionName,value in localOptions.items():
            if optionName == 'score':
                pass
            elif optionName == 'text':
                pass
            elif optionName =='feedback':
                tag = self.textTag('feedback',text=value,textFormat='MD')
            elif optionName =='tolerance':
                tag = self.valueTag('tolerance',value = value)

            if tag is not None : 
                answerTag.append(tag)

    
    def basicQuestion(self,tags,questionKind,**kwargs):
        
        # override default values in tags list
        for key, value in kwargs.items():
            if key in tags:
                content = tags[key]
                if content['kind'] == 'textTag':
                    content['values']['text'] = value
                else:
                    content['values']['value'] = value        
        
        # create basic question
        question = ET.Element('question',attrib={'type':questionKind})
        
        # insert tags in question
        for key,value in tags.items():
            func = eval('self.'+value['kind'])
            xmlTag = func(value['name'],**value['values'])
            question.append(xmlTag)   
            
        return question    
        
    def category(self,text,add=True,**kwargs):
        tags = {}
        tags['category']={'name':'category','kind':'textTag',\
                          'values':{'text':r'$module$/'+text,'textFormat':'txt'}}

        kwargs['questionKind']='category'
        question = self.basicQuestion(tags,**kwargs)

        if add : self.add(question)
        return question                        
                
        
    def buildQuestion(self,answers=(),add=True,**kwargs):
        questionKind = kwargs['questionKind']
        answerTextFormat='plain_text'
        
        tags = self.commonTagsList()        
                    
        # default tags
        if questionKind == 'multichoice':
            tags['shuffleanswers']={'name':'shuffleanswers','kind':'valueTag','values':{'value':1}}
            tags['answernumbering']={'name':'answernumbering','kind':'valueTag','values':{'value':'abc'}}
            answerTextFormat = 'MD'
        elif questionKind == 'numerical':
            answerTextFormat='plain_text'            
        elif questionKind == 'shortanswer':
            tags['usecase']={'name':'usecase','kind':'valueTag','values':{'value':0}}     
            answerTextFormat='plain_text'            
            
        question = self.basicQuestion(tags,**kwargs)
            
        for answer in answers:            
            answTag = self.textTag('answer',text=answer['text'],\
                            textFormat=answerTextFormat,\
                            attributes={'fraction':str(answer['fraction'])})
            # extra options available            
            options = answer['options'] if 'options' in answer else {}
            self.addAnswerOptions(questionKind,answTag,options)
            question.append( answTag)            
            
        if add : self.add(question)
        return question        
           
    
class moodleQuiz():
    def __init__(self,name=''):
        self.XML = moodleQuizXML()
        self.XML.category(name)
        
    def add(self,**kwargs):
        questionKind = kwargs['questionKind']
        answers = kwargs['answers']
        
        
        # calculate total points for fraction
        if questionKind == 'multichoice' or questionKind == 'shortanswer':
            totalPoints = 0
            for answer in answers:
                totalPoints+=answer['score']
            if totalPoints == 0:
                totalPoints = 1     
        else:
        # case of single answer
            answers[0]['score'] = True
            totalPoints = 1
        
        formattedAnswers = []        
        
        for answer in answers:
            value = (100 if answer['score'] else 0)/totalPoints
            ansData = {} ;  ansData.update(answer)
            ansData['fraction']=value
            formattedAnswers.append(ansData)
        kwargs['answers'] = formattedAnswers
        self.XML.buildQuestion(**kwargs)    
        
    def toString(self):
        header = '<?xml version="1.0" encoding="UTF-8"?>\n'        
        return header+self.XML.toString()
    
    def save(self,filename):
        open(filename,'w').write(self.toString())        

        
class answersClass(list):
    def __init__(self):
        pass
    
    def __iadd__(self,other):
        self.append(other)
        return self
        
    
    
        
def newQuestion(kind,name=''):
    Q = {}
    Q['questionKind'] = kind
    Q['name']    = name
    Q['text']    = ''
    Q['answers'] = answersClass()
    return Q