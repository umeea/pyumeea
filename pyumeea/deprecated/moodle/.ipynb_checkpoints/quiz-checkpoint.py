#
# This file is part of the PyEea package.
#
# Copyright (c) 2013-2020 PyMeasure Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import lxml.etree as ET
import lxml
import markdown2 as md
import codecs
import re
import base64
from bs4 import BeautifulSoup,NavigableString
import html


#import bs4.dammit
#import bs4.builder._htmlparser

# when analyzing spam, we always run into situations where the sender tries
# hard to obfuscate their input in order to sneak by spam detecting engines.
# instead of "ABC", they'll use HTML Entity references; &#65;&#66;&#67; in
# order to extract the viewer readable segments, we need a parser.  most
# parsers try to be smart and clean things up.  BeautifulSoup and lxml are
# two common parsers.  lxml is largely compiled C so we can't tweak it very
# easily.

# step #1
# BeautifulSoup insists on always doing entity substitution and there's no
# way to politely tell it to fuck off.  override the hex->int and
# word->symbol conversions, simply append our data to the growing stack
#_handle_data = bs4.builder._htmlparser.BeautifulSoupHTMLParser.handle_data
#bs4.builder._htmlparser.BeautifulSoupHTMLParser.handle_charref   = lambda cls,s: _handle_data(cls, '&#'+s+';')
#bs4.builder._htmlparser.BeautifulSoupHTMLParser.handle_entityref = lambda cls,s: _handle_data(cls, '&'+s+';')

# step #2
# BeautifulSoup insists on further ensuring printed data is always tidy and
# semantically correct, thus it ALWAYS does entity substitution even after
# we refused to do it above.  the below ensures the __str__ methods don't
# attempt to mangle the serialized data.  this simply returns the original
# matched input when the substitution methods are called
#bs4.dammit.EntitySubstitution._substitute_html_entity = lambda o: o.group(0)
#bs4.dammit.EntitySubstitution._substitute_xml_entity  = lambda o: o.group(0)





from os.path import basename, splitext


### b64  image loader
def encodeFile(filename):
    f=open(filename,'rb')
    svgdata = f.read()
    encoded = base64.b64encode(svgdata).decode("utf-8")
    f.close()
    return encoded

import unicodedata

class moodleQuizXML:    
    
    def __init__(self):
        self.Q = ET.Element('quiz')
    
    def add(self,inContent):
        self.Q.append(inContent)        

        
    def parseHTML(self,html,replaceLatex=True):
        soup = BeautifulSoup(html,"html.parser")#, from_encoding="utf-8")

        for tag in soup.findAll():
            if tag.name =='img':
                tag['src'] = "data:image/jpg;base64,"+encodeFile(tag['src'])    
            # treat all LaTeX $$ and $ sequences but avoid code sections
            elif tag.name != 'code':
                def mathReplacer(inBloc):
                    theStr = inBloc.group(0).replace('\n', '')
                    if theStr[0:2]=='$$':
                        newString = r'\['+theStr[2:-2]+r'\]'
                        return newString
                    elif theStr[0:1]=='$':
                        newString = r'\('+theStr[1:-1]+r'\)'
                        return newString
                for subtag in tag:
                    if isinstance(subtag,NavigableString):
                        newString = re.sub(r'(\${1,2})(?:(?!\1)[\s\S])*\1',mathReplacer,subtag) 
                        subtag.replace_with(newString)
                             
        return str(soup)   

        
        
    def textTag(self,tagName,text=None,textFormat='txt',attributes={}):
        localAttributes = {}
        html = None
        theText = text
        
        if textFormat.lower() == 'md' or textFormat == 'html':
            localAttributes['format'] = 'html'
        localAttributes.update(attributes)
                    
        mainTag = ET.Element(tagName,attrib = localAttributes)            
        textTag = ET.Element('text')

        if text is not None:        
            if textFormat.lower() == 'md':
                html = md.markdown(text,extras = ('tables','fenced-code-blocks','code-friendly'))
            elif textFormat.lower() == 'html':
                html = text
            
            if html is not None:
                html = self.parseHTML(html,True)
                theText = ET.CDATA(html)
                                   
            textTag.text = theText
                    
        mainTag.append(textTag)
        return mainTag
      
    def valueTag(self,tagName,value,attributes={}):
        vT = ET.Element(tagName,attrib=attributes)
        vT.text = str(value)
        return vT
    
    def booleanTag(self,tagName,value,attributes={}):
        bT = ET.Element(tagName,attrib=attributes)
        bT.text = 'true' if value else 'false'
        return bT
        
    def toString(self):
        return ET.tostring(self.Q,encoding='unicode', pretty_print=True)
    
    def basicQuestion(self,inType):
        question = ET.Element('question',attrib={'type':inType})
        return question
        
    def category(self,text,add=True):
        question = self.basicQuestion('category')
        question.append(self.textTag('category',text=r'$module$/'+text))
        if add : self.add(question)
        return question    
    
    def commonTagsList(self):
        tags={}
        tags['name']=[{'name':'name','kind':'textTag','values':{'text':'','textFormat':'txt'}}]
        tags['questiontext']=[{'name':'questiontext','kind':'textTag','values':{'text':'','textFormat':'MD'}}]
        tags['defaultgrade']=[{'name':'defaultgrade','kind':'valueTag','values':{'value':1.0}}]
        tags['generalfeedback']=[{'name':'generalfeedback','kind':'textTag','values':{'text':'','textFormat':'MD'}}]
        tags['penalty']=[{'name':'penalty','kind':'valueTag','values':{'value':0.1}}]
        tags['hidden']=[{'name':'hidden','kind':'valueTag','values':{'value':0}}]
        tags['booleanTag']=[{'name':'hidden','kind':'booleanTag','values':{'value':False}}]
        tags['shuffleanswers']=[{'name':'shuffleanswers','kind':'valueTag','values':{'value':1}}]
        tags['answernumbering']=[{'name':'answernumbering','kind':'valueTag','values':{'value':'abc'}}]
        return tags
                               
                
                
    def multiChoiceQuestion2(self,answers=(),add=True,**kwargs):
        tags = self.commonTagsList()
        
        # override default values
        for key, value in kwargs.items():
            if key in tags:
                content = tags[key]
                if content[key]['kind'] == 'textTag':
                    content[key]['values']['text'] = value
                else:
                    content[key]['values']['value'] = value
                    
        # insert in question
        question = self.basicQuestion("multichoice")
        for key,value in tags:
            func = eval('self.'+value['kind'])
            xmlTag = func(value['name'],**value['values'])
            question.append(xmlTag)
            
        for answer in answers:
            question.append(self.textTag('answer',text=answer['text'],textFormat='MD',attributes={'fraction':str(answer['fraction'])}))    
        if add : self.add(question)
        return question
        
    
    def multiChoiceQuestion(self,name='',text=None,answers=(),defaultgrade=1.0,generalfeedback=None,penalty=0.1,hidden=0,single=False,shuffleanswers=1,answernumbering='abc',add=True):
        question = self.basicQuestion("multichoice")
        
        question.append(self.textTag('name',text=name))
        question.append(self.textTag('questiontext',text=text,textFormat='MD'))
        question.append(self.valueTag('defaultgrade',value=defaultgrade))
        question.append(self.textTag('generalfeedback',text=generalfeedback,textFormat='MD'))
        
        question.append(self.valueTag('penalty',value=penalty))
        
        question.append(self.valueTag('hidden',value=hidden))
        
        question.append(self.booleanTag('single',value=single))
        question.append(self.valueTag('shuffleanswers',value=shuffleanswers))
        question.append(self.valueTag('answernumbering',value=answernumbering))
        for answer in answers:
            question.append(self.textTag('answer',text=answer['text'],textFormat='MD',attributes={'fraction':str(answer['fraction'])}))
        if add : self.add(question)
        return question
    
class moodleQuiz():
    def __init__(self,name=''):
        self.XML = moodleQuizXML()
        self.XML.category(name)
        
    def addQCM(self,name='',text=None,answers=(),defaultgrade=1.0,generalfeedback=None,penalty=0.1,hidden=0,single=False,shuffleanswers=1,answernumbering='abc'):
        formattedAnswers = []
        
        totalPoints = 0
        for answer in answers:
            totalPoints+=answer[0]
        if totalPoints == 0:
            totalPoints = 1
            
            
        for answer in answers:
            value = (100 if answer[0] else 0)/totalPoints
            formattedAnswers.append({'text':answer[1],'fraction':value})
        self.XML.multiChoiceQuestion(name=name,text=text,answers=formattedAnswers,defaultgrade=defaultgrade,generalfeedback=generalfeedback,penalty=penalty,hidden=hidden,single=single,shuffleanswers=shuffleanswers,answernumbering=answernumbering)
        
        
        
    def addQCM2(self,**kwargs):
        formattedAnswers = []
        
        totalPoints = 0
        answers = kwargs['answers']
        for answer in answers:
            totalPoints+=answer[0]
        if totalPoints == 0:
            totalPoints = 1
            
            
        for answer in answers:
            value = (100 if answer[0] else 0)/totalPoints
            formattedAnswers.append({'text':answer[1],'fraction':value})
        kwargs['answers'] = formattedAnswers
        self.XML.multiChoiceQuestion2(**kwargs)
        
        

    def toString(self):
        
        header = '<?xml version="1.0" encoding="UTF-8"?>\n'        
        return header+self.XML.toString()
    
    def save(self,filename):
        open(filename,'w').write(self.toString())        
        
        
def newQuestion(name=''):
    Q = {}
    Q['name']=name
    Q['text']=''
    Q['answers']=[]
    return Q