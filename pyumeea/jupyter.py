def eeaConfig():
    import matplotlib.pyplot as plt
    plt.rcParams['figure.max_open_warning'] = 0
    #from IPython.core.interactiveshell import InteractiveShell
    #InteractiveShell.ast_node_interactivity = "none"  # not good for Cython


    try:
        from IPython import get_ipython
        shellName = get_ipython().__class__.__name__
    except NameError:
        shellName = ''

    if shellName == 'ZMQInteractiveShell':
        from .utils import plotMode
        plotMode('ecran')
        get_ipython().run_line_magic("load_ext", "Cython")

