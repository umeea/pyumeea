import matplotlib.pyplot as plt
from IPython.core.display import display

import numpy as np
import sympy as sp


def symToNum(H,components,f=None):
    fmin = 1;fmax = 1e9;npts = 1000

    if isinstance(f, (list,tuple) ):
        if   len(f) == 1 : npts = f[0]
        elif len(f) == 2 : fmin = f[0]; fmax = f[1]
        elif len(f) == 3 : fmin = f[0]; fmax = f[1];npts = f[2]
    if f is None or len(f) <=3  : f=np.logspace(np.log10(fmin),np.log10(fmax),npts)
    else: f = np.array(f)

    theSymVars = H.free_symbols

    Hfn = sp.lambdify(theSymVars,H)

    theNumVars = {'s':1j*2*np.pi*f}

    for k,v in components.items():
        if isinstance(k,str):
            theNumVars[k]=v
        elif isinstance(k,sp.Symbol):
            theNumVars[k.name]=v
        else:
            raise ValueError("Liste de composants incorrecte !")

    Hn = Hfn(**theNumVars)

    return f,Hn

def buildInitialFigure(nbSubplots):
    figure,*_=plt.subplots(nbSubplots,1,figsize=(7,7))
    return figure

def plotArrows(x,y,ax,color,nbArrows):
    plt.sca(ax)

    dx = np.gradient(x)
    dy = np.gradient(y)
    allLengths = np.cumsum(np.sqrt(dx**2 + dy**2) )
    posArrows=[]
    shortLen = allLengths[-1]/(nbArrows+1)
    actLen = 0
    for k in range(nbArrows):
        posArrows.append( np.abs(allLengths - shortLen*(k+1)).argmin())

    for k in posArrows:
        plt.arrow(x[k], y[k], dx[k],dy[k], shape='full', lw=0, length_includes_head=True, head_width=allLengths[-1]*0.01,color=color)


def nBode(f=None,Hn=None,figure=None,label=None,style={}):
    if f is None or Hn is None or figure is None:
        figure = buildInitialFigure(2)


    if f is None or Hn is None:
        return figure

    ax1 = figure.axes[0]
    ax2 = figure.axes[1]

    plt.sca(ax1)
    plt.axhline(y=0, color='r', linestyle='--')
    plt.plot(f,20*np.log10(np.abs(Hn)),label=label if  label is not None else 'module',**style)
    plt.sca(ax1)
    plt.xscale('log')
    plt.xlabel('Fréquence (Hz)')
    plt.ylabel('Module (dB)')
    plt.grid(True);
    plt.legend()

    plt.sca(ax2)
    plt.axhline(y=-180, color='r', linestyle='--')
    plt.plot(f,np.unwrap(np.angle(Hn))*180/np.pi,label=label if  label is not None else 'phase',**style)
    plt.xscale('log')
    plt.xlabel('Fréquence (Hz)')
    plt.ylabel('Déphasage (°)')
    plt.grid(True);
    plt.legend()


    return figure


def sBode(H=None,components=None,f=None,figure=None,label=None,style={}):

    if H is None or components is None or figure is None:
        figure = buildInitialFigure(2)

    if H is None or components is None:
        return figure

    f,Hn = symToNum(H,components,f)

    print("ICI")
    return nBode(f,Hn,figure=figure,label=label,style=style)


def nNyquist(f=None,Hn=None,figure=None,label=None,nbArrows=3,style={}):
    if Hn is None or figure is None:
        figure = buildInitialFigure(1)

    if Hn is None:
        return figure

    x = np.real(Hn)
    y = np.imag(Hn)

    ax1 = figure.axes[0]

    plt.sca(ax1)

    p = plt.plot(x ,y ,label=label if  label is not None else 'lieu',**style)
    #plt.axhline(y=0, color='r', linestyle='--')
    #plt.axvline(x=-1, color='r', linestyle='--')
    plt.plot(-1,0,'rx',label='_invisible')
    plt.xlabel('Partie Réelle')
    plt.ylabel('Partie Imaginaire')
    plt.grid(True);
    plt.legend()

    plotArrows(x,y,ax1,color=p[-1].get_color(),nbArrows = nbArrows)

    return figure


def sNyquist(H=None,components=None,label=None,figure=None,f=None,nbArrows=3,style={}):

    if H is None or components is None or figure is None:
        figure = buildInitialFigure(1)

    if H is None or components is None:
        return figure

    f,Hn = symToNum(H,components,f)

    return nNyquist(f,Hn,figure=figure,label=label,nbArrows=nbArrows,style=style)


def nBlack(f=None,Hn=None,figure=None,label=None,nbArrows=3,style={}):
    if Hn is None or figure is None:
        figure = buildInitialFigure(1)

    if Hn is None:
        return figure

    x = np.unwrap(np.angle(Hn))*180/np.pi
    y = 20*np.log10(np.abs(Hn))

    ax1 = figure.axes[0]

    plt.sca(ax1)
    p = plt.plot(x ,y ,label=label if  label is not None else 'lieu',**style)
    plt.plot(-180,0,'rx',label='_invisible')
    
    plt.xlabel('Phase (°)')
    plt.ylabel('Module (dB)')
    plt.grid(True);
    plt.legend()

    plotArrows(x,y,ax1,color=p[-1].get_color(),nbArrows = nbArrows)

    return figure


def sBlack(H=None,components=None,label=None,figure=None,f=None,nbArrows=3,style={}):

    if H is None or components is None or figure is None:
        figure = buildInitialFigure(1)

    if H is None or components is None:
        return figure

    f,Hn = symToNum(H,components,f)

    return nBlack(f,Hn,figure=figure,label=label,nbArrows=nbArrows,style=style)

# documentation section
from ..institution.widgets import eeaWidget
from ..docGenerator import docBuilder

credits = 'v 1.0 - par M. Myara, F. Martinez et S. Parola - Département EEA / IES UMR5214 - Université de Montpellier'
imports = {'bottomBanner':eeaWidget(credits=credits,mode='htmlCode')}
    
def symToNumDoc():
    docFile = ('circuits','data','symToNumDoc.md')        
    docBuilder(docFile,imports)

def sBodeDoc():
    docFile = ('circuits','data','sBodeDoc.md')        
    docBuilder(docFile,imports)

def sNyquistDoc():
    docFile = ('circuits','data','sNyquistDoc.md')           
    docBuilder(docFile,imports)
    
def sBlackDoc():
    docFile = ('circuits','data','sBlackDoc.md')            
    docBuilder(docFile,imports)
                
def nBodeDoc():
    docFile = ('circuits','data','nBodeDoc.md')        
    docBuilder(docFile,imports)

def nNyquistDoc():
    docFile = ('circuits','data','nNyquistDoc.md')           
    docBuilder(docFile,imports)
    
def nBlackDoc():
    docFile = ('circuits','data','nBlackDoc.md')            
    docBuilder(docFile,imports)
                
