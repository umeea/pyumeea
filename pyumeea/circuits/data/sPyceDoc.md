## sPyce() Documentation

### Description  
`sPyce()` est une fonction Python qui calcule les tensions et courants d'un circuit électronique *(tensions des noeuds + courants)* en exploitant la méthode *MNA : Modified Nodal Analysis*. Le circuit est décrit sous la forme d'un fichier 'netlist'. La fonction prend un seul paramètre, le nom du fichier netlist sous forme d'une chaine de caratères. Un appel à la fonction se fera typiquement comme ci-dessous :  
```python
e,varsDict,MNA,RHS = sPyce(netlist,workspace)
```

On donne :  
- `netlist` : la netlist à traiter sous forme d'une chaine de caractères qui représente directement la *Netlist*.  
- `workspace` : est le lieu dans lequel sPyce devra créer les variables symboliques manquantes. Il sera toujours égal à `globals()`

Et on obtient en retour :  
- `e` : les tensions des noeuds et courants résolus  
- `varsDict` : les noms des variables qui ont été résolues sous forme d'un dictionnaire python  
- `MNA`  et `RHS`  : la matrice MNA et le veteur RHS qui ont servi à résoudre le circuit

Si on ne veut récupérer que `e` et `varsDict` par exemple, l'appel pourra se faire de la façon suivante :  
```python
e,varsDict,*_ = sPyce(netlist,workspace)
```  

 
*Note : si la netlist est contenue dans un fichier texte, il suffit de la charger comme suit :*  
```python
netlist = open('nomDuFichier.txt','r').read()
e,varsDict,*_ = sPyce(netlist,workspace)
```

Il reste à décrire les netlists elles-mêmes.

### Netlist
Un fichier Netlist est un fichier texte dans lequel chaque ligne correspond à un composant du circuit. Pour chaque composant, on donne son placement dans le circuit en définissant à quels noeuds il est connecté, ainsi qu'un ensemble de paramètres (par exemple : pour une résistance on indique sa valeur). Pour la plupart des composants, la syntaxe est du type :  
`ComposantIdentifiant N1 N2 .... Nn Valeur`  

où :  
- `Composant` est une simple lettre qui représente le type de composant,  
- `Identifiant` identifie le composant, les `N1 N2 .... Nn`sont les noeuds où est placé le composant, et   
- `Valeur` est la valeur du composant. Par exemple, la ligne correspondant à la résistance numéro 2 d'un circuit, qui serait située entre les noeuds 3 et 5 et dont la valeur serait $10\,k\Omega$, la ligne netlist serait :
```
R2 3 5 10e3
```

La syntaxe précise pour chaque type de composant est donnée dans la première colonne du tableau ci-dessous :  

<!--import id="displayStampsList"  /-->


### Exemple avec un circuit passif
Les noeuds du circuit ont été numérottés (valeurs entre parenthèses sur le schéma), avec la masse toujours identifiée comme noeud *(0)*. L'ordre des lignes n'a pas d'importance et ne change ni la matrice MNA, ni les tensions des noeuds. On donne les valeurs : $R_1=R_B$, $R_2=R_3=R_A$, $C_1=C_B$, $C_2=C_A$.   


#### Le Circuit

<!--img alt="Circuit RC" width="300px" src="circuits/data/sPyceDoc_RC01.png" /-->



#### Construction de la Netlist

| Composant| Référence |   | Noeuds | Valeur numérique<br> ou symbolique |
|--------|---------|-|--------|------------------------------------|
|        V | 1         |   |   0 1  | Ve                                 |
|        R | 1         |   |   1 2  | RB                                 |
|        C | 1         |   |   2 3  | CB                                 |
|        C | 2         |   |   3 4  | CA                                 |
|        R | 3         |   |   4 0  | RA                                 |
|        R | 2         |   |   3 4  | RA                                 |

#### Netlist finalisée
```
V1 0 1 Ve
R1 1 2 RB
C1 2 3 CB
C2 3 4 CA
R3 4 0 RA
R2 3 4 RA
```

#### Programme d'exemple

**Calcul des inconnues du circuit :**  
On aura besoin de calcul symbolique, de calcul numérique, de tracer dess courbes, et bien sûr de sPyce qui est contenu dans pyeea.circuits :  

```python
from pyeea.circuits import *

netlist = """
V1 0 1 V_e
R1 1 2 RB
C1 2 3 CB
C2 3 4 CA
R2 3 4 RA
R3 4 0 RA
"""

e,inc,*_ = sPyce(netlist,globals())

e.simplify()
display(e)
display(inc)

```  

On a obtenu dans `e` les tensions et courants résolus,  dans `inc` la liste des inconnues sous la forme d'un dictionnaire python. Les autres résultats (matrices qui ont permis de résoudre le circuit) n'ont pas été affichés ici.  
&nbsp;  

 
**Fonction de transfert :**  
Il suffit de réaliser la division entre la tension du noeud de sortie (ici le 4) et la tension du noeud d'entrée (ici le 1).
```python
Hs = e[inc['V(4)']]/e[inc['V(1)']]
Hs = Hs.simplify()
print("Fonction de transfert symbolique dans l'espace de Laplace :")
display(Hs)
```  

Et on obtient :  
$$ \frac{CB RA s \left(CA RA s + 1\right)}{CA CB RA^{2} s^{2} + CA CB RA RB s^{2} + CA RA s + 2 CB RA s + CB RB s + 1}$$
 

On peut aussi vouloir l'afficher dans l'espace de Fourier, ce qui revient à remplacer $s$ par $j\omega$ :
```python
sp.var('w ',domain='positive')
Hw = Hs.subs({s:sp.I*w}).simplify()
print("Fonction de transfert symbolique dans l'espace de Fourier :")
display(Hw)

``` 

Et on obtient :
$$\frac{CB RA w \left(CA RA w - i\right)}{CA CB RA^{2} w^{2} + CA CB RA RB w^{2} - i CA RA w - 2 i CB RA w - i CB RB w - 1}$$
 
&nbsp;

**Diagramme de Bode :**  
Pour tracer le diagramme de Bode, on a besoin d'affecter des valeurs aux composants. On peut utiliser la fonction `sBode` de la bibliothèque, qui fait ce travail et définit par défaut un axe des fréquences de $1\,Hz$ à $1\,GHz$. On va faire ce tracé ici pour un jeu de composants :

```python
sBode(Hs,{'RA':1e3,'RB':10e3,'CA':1e-7,'CB':1e-9})
```
Et on obtient :  
<!--img alt="Circuit RC" width="600px" src="circuits/data/sPyceDoc_Bode.png" /-->


**Notes :**  
- on peut paramétrer `sBode()` pour choisir un axe des fréquences différent. Pour en savoir plus, exécuter la fonction `sBodeDoc()`  
- On peut aussi tracer plusieurs diagrammes de Bode (pour des jeux de composants différents par exemple) sur la même figure. Pour en savoir plus, exécuter la fonction `sBodeDoc()`  
- on peut aussi récupérer les valeurs numériques de la fonction de transfert avec la fonction `symToNum()`. Pour en savoir plus, exécuter la fonction `symToNumDoc()`  
- on dispose aussi de fonctions similaires à `sBode()` pour tracer des diagrammes de Nyquist ou de Black. Elles se nomment `sNyquist()` et `sBlack()`. Leur documentation est accessible via `sNyquistDoc()`et `sBlackDoc()`  
- il existe aussi des versions qui tracent des diagrammes de Bode, Nyquist et Black à partir des valeurs numériques de la fonction de transfert. Elles se nomment `nBode()` , `nNyquist()` et `nBlack()`, et sont documentées respectivement dans `nBodeDoc()` , `nNyquistDoc()` et `nBlackDoc()`