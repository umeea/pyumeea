## sBlack() Documentation

### Description rapide
`sBlack()` est une fonction Python qui affiche le diagramme de Black d'une fonction de transfert **symbolique** écrite avec la variable de Laplace `s`. Les paramètres principaux à lui fournir sont la fonction de transfert et les valeurs des composants. L'appel le plus simple que l'on puisse faire à cette fonction est donné ci-dessous :

```python
import sympy as sp
from pyeea.circuits import *
sp.var('s,R,C')
Hs = 1/(1+R*C*s)

sBlack(Hs,{R:1e3,C:1e-8})
```
Et l'on obtient :

<!--img alt="Circuit RC" width="600px" src="circuits/data/sBlackDoc_plot.png" /-->

### Description complète
Pour une utilisation plus avancée, le fonctionnement de `sBlack()` est exactement le même que celui de `sBode()`. Il suffit donc de prendre les exemples de `sBodeDoc()` en remplaçant les appels à `sBode()` par des appels à `sBlack()`