## nBode() Documentation

### Description rapide
`nBode()` est une fonction Python qui affiche le diagramme de Bode d'une fonction de transfert écrite avec la variable de Laplace, donnée en paramètre, et des valeurs des composants. L'appel le plus simple que l'on puisse faire à cette fonction est donné ci-dessous :

```python
import numpy as np
from pyeea.circuits import *

f = np.logspace(0,9,1000)

R = 1e3; C = 1e-8
Hn = 1/(1+2*1j*np.pi*R*C*f)

nBode(f,Hn)
```
Et l'on obtient :

<!--img alt="Circuit RC" width="600px" src="circuits/data/sBodeDoc_bode.png" /-->

### Description complète
La fonction est définie ainsi :
```python
nBode(f=None,Hn=None,figure=None,label=None,style={})
```
Les paramètres sont :  

- `f` est un un tableau contenant la liste des fréquences
- `Hn` est la fonction de transfert numérique complexe
- `figure` : une figure existante à remplir avec le diagramme de Bode. Utile pour tracer plusieurs diagrammes sur la même figure.  
- `label` : un nom à indiquer dans la légende.
- `style`: permet de modifier le syle du tracé conformément à la syntaxe de Matplotlib.pyplot

&nbsp;
Enfin, si on veut juste créer une figure vierge pour le diagramme de Bode, on peut simplement écrire :
```python
figBode = nBode()
```

Par exemple, si on veut tracer 3 diagrammes de Bode de $20\,Hz$ à $500\,kHz$ sur 15 points, pour 3 résistances ( $1 k\Omega$,$3 k\Omega$ et $10 k\Omega$), on peut écrire :

```python
import sympy as sp
from pyeea.circuits import *

f = np.logspace(0,9,1000)

R1 = 1e3; C1 = 1e-8
Hn1 = 1/(1+2*1j*np.pi*R1*C1*f)
R2 = 3e3; C2 = 1e-8
Hn2 = 1/(1+2*1j*np.pi*R2*C2*f)
R3 = 10e3; C3 = 1e-8
Hn3 = 1/(1+2*1j*np.pi*R3*C3*f)


figBode = nBode()
nBode(f,Hn1,figure=figBode,label='1 kOhm')
nBode(f,Hn2,figure=figBode,label='3 kOhm')
nBode(f,Hn3,figure=figBode,label='10 kOhm')
```

