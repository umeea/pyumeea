## symToNumDoc() Documentation

### Description rapide
`symToNum()` est une fonction Python qui échantillonne une fonction symbolique dépendant de la variable de Laplace `s` et des valeurs des autres valeurs du circuit. Elle renvoie un tableau pour l'axe des fréquences et un tableau qui contient la fonction de transfert complexe échantillonnée. Sur un exemple (on n'affiche que quelques valeurs):

```python
import sympy as sp
from pyeea.circuits import *
sp.var('s,R,C')
Hs = 1/(1+R*C*s)

f,Hn = symToNum(Hs,{R:1e3,C:1e-8})
print('f=',f[0:999:50],'\n')
print('Hn=',Hn[0:999:50])
```
On obtient :
```
f= [1.00000000e+00 2.82130768e+00 7.95977700e+00 2.24569800e+01
 6.33580499e+01 1.78752553e+02 5.04315949e+02 1.42283046e+03
 4.01424249e+03 1.13254132e+04 3.19524751e+04 9.01477631e+04
 2.54334576e+05 7.17556092e+05 2.02444651e+06 5.71158648e+06
 1.61141428e+07 4.54629547e+07 1.28264983e+08 3.61874981e+08] 

Hn= [9.99999996e-01-6.28318528e-05j 9.99999969e-01-1.77267984e-04j
 9.99999750e-01-5.00127414e-04j 9.99998009e-01-1.41101086e-03j
 9.99984153e-01-3.98084060e-03j 9.99873873e-01-1.12299375e-02j
 9.98996934e-01-3.16553213e-02j 9.92071174e-01-8.86902446e-02j
 9.40188865e-01-2.37136593e-01j 6.63847591e-01-4.72391752e-01j
 1.98783795e-01-3.99084951e-01j 3.02273642e-02-1.71212355e-01j
 3.90060639e-03-6.23329099e-02j 4.91716690e-04-2.21692333e-02j
 6.18017552e-05-7.86116631e-03j 7.76467670e-06-2.78650613e-03j
 9.75495848e-07-9.87671452e-04j 1.22553267e-07-3.50076067e-04j
 1.53965721e-08-1.24082923e-04j 1.93429694e-09-4.39806428e-05j]
```


### Description complète
La fonction est définie ainsi :
```python
symToNum(H,components,f=None)
```
Les paramètres sont :  

- `H` est la fonction de transfert symbolique, écrite avec la variable de Laplace
- `components` est un dictionnaire qui fait l'association entre les variables de la fonction de transfert et les valeurs qui serviront à tracer la courbe
- `f` est un `tuple`, une `list` ou une `ndarray` de numpy qui contient des informations sur la plage de fréquences demandée :
    - si ce paramètre est omis, la situation standard est de tracer le diagramme de Bode de $1\,Hz$ à $1\,GHz$ sur 1000 points avec une échelle multiplicative (bien adaptée pour un tracé en échelle logarithmique).  
    - si `f` est constitué d'une seule valeur, cette valeur est le nombre de points.
    - si `f` contient 2 valeurs, elles sont interprétées comme la fréquence min et la fréquence max (dans cet ordre) 
    - si `f` contient 3 valeurs, elles sont interprétées comme fréquence min, max et nombre de points (dans cet ordre)  
    - au dela : la fonction de transfert sera échantillonnée pour les valeurs de fréquences données. Dans ce dernier cas, le vecteur des fréquences renvoyé correspond à celui qui est fourni à la fonction.  
    
    
Ainsi, on peut choisir d'échantillonner la fonction de transfert autrement, par exemple :
```python
import sympy as sp
import numpy as np
from pyeea.circuits import *
import matplotlib.pyplot as plt
sp.var('s,R,C')
Hs = 1/(1+R*C*s)

freq = np.linspace(10,1000,30)

f,Hn = symToNum(Hs,{R:1e3,C:5e-7},f=freq)
print('f=',freq,'\n')
print('Hn=',Hn)

plt.figure()
plt.plot(freq,np.abs(Hn))
```
On obtient :  
```
f= [  10.           44.13793103   78.27586207  112.4137931   146.55172414
  180.68965517  214.82758621  248.96551724  283.10344828  317.24137931
  351.37931034  385.51724138  419.65517241  453.79310345  487.93103448
  522.06896552  556.20689655  590.34482759  624.48275862  658.62068966
  692.75862069  726.89655172  761.03448276  795.17241379  829.31034483
  863.44827586  897.5862069   931.72413793  965.86206897 1000.        ] 

Hn= [0.99901401-0.03138495j 0.98113519-0.13604754j 0.94297619-0.2318881j
 0.88910953-0.31399645j 0.82510054-0.37988109j 0.75629792-0.42931501j
 0.68705335-0.46369283j 0.62044136-0.48527712j 0.55833958-0.49658483j
 0.50168122-0.49999717j 0.45073986-0.49756752j 0.40537436-0.49096435j
 0.36521134-0.48148937j 0.32976857-0.47012898j 0.29853217-0.45761415j
 0.27100125-0.44447674j 0.24671127-0.43109723j 0.22524418-0.41774303j
 0.20623083-0.40459816j 0.18934915-0.39178572j 0.17432031-0.37938468j
 0.16090395-0.36744234j 0.14889343-0.35598339j 0.1381112 -0.34501666j
 0.12840471-0.3345399j  0.11964279-0.32454336j 0.11171243-0.31501232j
 0.10451613-0.30592893j 0.09796957-0.2972735j  0.09199967-0.28902548j]
```

<!--img alt="Circuit RC" width="600px" src="circuits/data/symToNumDoc_plot.png" /-->


### Fonctionnement interne
En simplifiant, le code interne de la fonction revient à :
```python
import numpy as np
import sympy as sp
sp.var('s,R,C')
Hs = 1/(1+R*C*s)

# récupérer la liste des symboles dans la fonction de transfert
varsSym = Hs.free_symbols   

# Après appel à lambdiy, Hfn sera une fonction python
# image de la fonction de transfert symbolique Hss
Hfn = sp.lambdify(varsSym,Hs)

# definition des valeurs numériques
f = np.logspace(0,9,1000)
varsNum = {'R':1e3,'C':1e-9,'s':1j*2*np.pi*f}

# utilisation de la fonction Hfn pour obtenir les valeurs numériques
Hn = Hfn(**varsNum)

print('f=',f[0:999:50],'\n')
print('Hn=',Hn[0:999:50])
```
