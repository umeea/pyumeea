## sBode() Documentation

### Description rapide
`sBode()` est une fonction Python qui affiche le diagramme de Bode  d'une fonction de transfert **symbolique** écrite avec la variable de Laplace `s`. Les paramètres principaux à lui fournir sont la fonction de transfert et les valeurs des composants. L'appel le plus simple que l'on puisse faire à cette fonction est donné ci-dessous :

```python
import sympy as sp
from pyeea.circuits import *
sp.var('s,R,C')
Hs = 1/(1+R*C*s)

sBode(Hs,{R:1e3,C:1e-8})
```
Et l'on obtient :

<!--img alt="Circuit RC" width="600px" src="circuits/data/sBodeDoc_bode.png" /-->

### Description complète
La fonction est définie ainsi :
```python
sBode(H=None,components=None,f=None,figure=None,label=None,style={})
```
Les paramètres sont :  

- `H` est la fonction de transfert symbolique, écrite avec la variable de Laplace
- `components` est un dictionnaire qui fait l'association entre les variables de la fonction de transfert et les valeurs qui serviront à tracer la courbe
- `f` est un `tuple`, une `list` ou une `ndarray` de numpy qui contient des informations sur la plage de fréquences demandée :
    - si ce paramètre est omis, la situation standard est de tracer le diagramme de Bode de $1\,Hz$ à $1\,GHz$ sur 1000 points avec une échelle multiplicative (bien adaptée pour un tracé en échelle logarithmique).  
    - si `f` est constitué d'une seule valeur, cette valeur est le nombre de points.
    - si `f` contient 2 valeurs, elles sont interprétées comme la fréquence min et la fréquence max (dans cet ordre) 
    - si `f` contient 3 valeurs, elles sont interprétées comme fréquence min, max et nombre de points (dans cet ordre)  
    - au dela : la fonction de transfert sera échantillonnée pour les valeurs de fréquences données.  
- `figure` : une figure existante à remplir avec le diagramme de Bode. Utile pour tracer plusieurs diagrammes sur la même figure.  
- `label` : un nom à indiquer dans la légende.
- `style`: permet de modifier le syle du tracé conformément à la syntaxe de Matplotlib.pyplot

&nbsp;
Enfin, si on veut juste créer une figure vierge pour le diagramme de Bode, on peut simplement écrire :
```python
figBode = sBode()
```

Par exemple, si on veut tracer 3 diagrammes de Bode de $20\,Hz$ à $500\,kHz$ sur 15 points, pour 3 résistances ( $1 k\Omega$,$3 k\Omega$ et $10 k\Omega$), on peut écrire :

```python
import sympy as sp
from pyeea.circuits import *
sp.var('s,R,C')
Hs = 1/(1+R*C*s)

figBode = sBode()
sBode(Hs,{R:1e3,C:1e-8},f=(20,500e3,15),figure=figBode,label='1 kOhm')
sBode(Hs,{R:3e3,C:1e-8},f=(20,500e3,15),figure=figBode,label='3 kOhm')
sBode(Hs,{R:10e3,C:1e-8},f=(20,500e3,15),figure=figBode,label='10 kOhm')

```
ou encore :
```python
import sympy as sp
from pyeea.circuits import *
sp.var('s,R,C')
Hs = 1/(1+R*C*s)

figBode = sBode()
Rvals = (1e3,3e3,10e3)
for Rv in Rvals:
    sBode(Hs,{R:Rv,C:1e-8},f=(20,500e3,15),figure=figBode,label='%.0f kOhm'%(Rv/1000))
```

Pour les deux exemples, on obtient :

<!--img alt="Circuit RC" width="600px" src="circuits/data/sBodeDoc_bode2.png" /-->

### Fonctionnement interne
En interne, `sBode` utilise :  

- d'abord `symToNum` pour convertir la fonction de transfert symbolique en valeurs numériques
- puis `nBode` pour le tracé des courbes.

### Tracés expérimentaux et théoriques
On peut mélanger l'utilisation de `sBode()` et `nBode()` pour cela :
```python
import sympy as sp
import numpy as np
from pyeea.circuits import *
# Theorie
sp.var('s,R,C')
Hs = 1/(1+R*C*s)

# Experience
## releves
f_exp=np.array([1,10,100,1e3,3e3,5e3,10e3,30e3,50e3,100e3,300e3,500e3,1e6,1e7])
module_exp = np.array([1.        , 0.98 ,0.97, 0.99, 0.97,0.94, 0.82, 0.55, 0.3, 0.17,0.05, 0.025, 0.012, 0.002])
phase_exp = np.array([0, - 0.1, -0.1, -5,-10, -19, -30, -60,-68, -81, -88, -88,-89, -90])

## calcul de la fonction de transfert expérimentale complexe
H_exp = module_exp * np.exp( 1j * phase_exp *np.pi / 180)

#Traces
figBode = sBode(Hs,{R:1e3,C:1e-8},label='theorie')
nBode(f_exp, H_exp, figure = figBode, style={'marker':'+','linewidth':0}, label='experience')

```

on obtient :

<!--img alt="Circuit RC" width="600px" src="circuits/data/sBodeDoc_bode3.png" /-->