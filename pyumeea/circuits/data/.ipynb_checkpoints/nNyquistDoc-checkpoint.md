## nNyquist() Documentation

### Description rapide
`nNyquist()` est une fonction Python qui affiche le diagramme de Nyquist d'une fonction de transfert écrite avec la variable de Laplace, donnée en paramètre, et des valeurs des composants. L'appel le plus simple que l'on puisse faire à cette fonction est donné ci-dessous :

```python
import numpy as np
from pyeea.circuits import *

f = np.logspace(0,9,1000)

R = 1e3; C = 1e-8
Hn = 1/(1+2*1j*np.pi*R*C*f)

nNyquist(f,Hn)
```
Et l'on obtient :

<!--img alt="Circuit RC" width="600px" src="circuits/data/nNyquistDoc_plot.png" /-->

### Description complète
Pour une utilisation plus avancée, le fonctionnement de `nNyquist()` est exactement le même que celui de `nBode()`. Il suffit donc de prendre les exemples de `nBodeDoc()` en remplaçant les appels à `nBode()` par des appels à `nNyquist()`