# SPyce software for python (symbolic version, for linear circuits)
# by M. Myara, F. Martinez and S. Parola, Departement EEA, Montpellier, France
# v 1.0, June 2020


#### define Laplace variable
import sympy as sp
import os.path
from IPython.core.display import display, HTML,Markdown
from ..institution.widgets import eeaWidget
from ..docGenerator import encodeFile,buildMarkdownTable,docBuilder


class workspaceMgr:
    def __init__(self,externalWorkspace):
        self.localSymbols=set()
        self.externalWorkspace = externalWorkspace
        self.buildMissingVars('s')
    def __del__(self):
        self.cleanLocalSymbols()

    def buildMissingVars(self,expressionString):
        localVars = sp.sympify(expressionString,locals={}).free_symbols
        for var in localVars:
            name = var.name
            globals().update({name:var})
            self.localSymbols.update({name})
            if name not in self.externalWorkspace:
                self.externalWorkspace.update({name:var})
                sp.var(name)

    def cleanLocalSymbols(self):
        for var in self.localSymbols:
            if var in globals():
                del globals()[var]
        self.localSymbols = set()

class stampsMgr:



##### functions that generate stamps for each kind of component

    def helper(deviceParams,expectedParamsAmount,stampTemplate,rhsTemplate,nbvals=1,workspace= workspaceMgr({}) ):
        # nbvals should be 0 or 1
        successful = True;  nodes=[];   stamp=stampTemplate;   rhs=rhsTemplate;

        if len(deviceParams)== expectedParamsAmount:
            nodes = [r'V(%s)' % net for net in deviceParams[:expectedParamsAmount-nbvals] ]
            for k in range(expectedParamsAmount-nbvals,expectedParamsAmount):
                value = deviceParams[k]
                workspace.buildMissingVars(value)

            n = stampTemplate.count(r'%s');
            if n!=0:
                l=[value for el in range(n)]
                stamp =  stampTemplate % tuple(l)
            n = rhsTemplate.count(r'%s')
            if n!=0:
                l=[value for el in range(n)]
                rhs = rhsTemplate % tuple(l)
            stamp = eval(r'sp.Matrix('+ stamp + ')')
            rhs = eval(r'sp.Matrix('+ rhs + ')')
        return successful,nodes,stamp,rhs

    def currentSource(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[0,0],[0,0]]'; rhsTemplate = '[[-(%s)],[%s]]'
        extra = []
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,3,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def voltageSource(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[0,0,1],[0,0,-1],[1,-1,0]]'; rhsTemplate = '[[0],[0],[%s]]'
        extra = [r'I(%s)' % device ]
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,3,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def resistor(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[1/(%s),-1/(%s)],[-1/(%s),1/(%s)] ]';rhsTemplate = '[[0],[0]]';
        extra = []
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,3,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def self(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[1/(%s*s),-1/(%s*s)],[-1/(%s*s),1/(%s*s)] ]';rhsTemplate = '[[0],[0]]'
        extra = []
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,3,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def capacitor(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[ [%s*s,-%s*s],[-%s*s,%s*s] ]';rhsTemplate = '[[0],[0]]'
        extra = []
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,3,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def VCVS(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[0,0,0,0,1],[0,0,0,0,-1],[0,0,0,0,0],[0,0,0,0,0],[1,-1,-(%s),%s,0]]'
        rhsTemplate = r'[[0],[0],[0],[0],[0]]'
        extra = [r'I(%s)' % device ]
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,5,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def CCCS(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[0,0,0,0,%s],[0,0,0,0,-(%s)],[0,0,0,0,1],[0,0,0,0,-1],[0,0,1,-1,0]]'
        rhsTemplate = r'[[0],[0],[0],[0],[0]]'
        extra = []
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,5,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def VCCS(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[0,0,%s,-(%s)],[0,0,-(%s),%s],[0,0,0,0],[0,0,0,0]]'
        rhsTemplate = r'[[0],[0],[0],[0]]'
        extra = []
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,5,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def CCVS(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[0,0,0,0,1,0],[0,0,0,0,-1,0],[0,0,0,0,0,1],[0,0,0,0,0,-1],[1,-1,0,0,0,-(%s)],[0,0,1,-1,0,0]]'
        rhsTemplate = r'[[0],[0],[0],[0],[0],[0]]'
        extra = [r'I(%s)' % device ,r'IC(%s)' % device]
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,5,stampTemplate,rhsTemplate,workspace=workspace)
        return successful,nodes,extra,stamp,rhs
    def IdealOpAmp(device,deviceParams,workspace=workspaceMgr({}) ):
        stampTemplate = r'[[0,0,0,1],[0,0,0,0],[0,0,0,0],[0,1,-1,0]]'
        rhsTemplate = r'[[0],[0],[0],[0]]'
        extra = [r'I(%s)' % device ]
        successful,nodes,stamp,rhs=stampsMgr.helper(deviceParams,3,stampTemplate,rhsTemplate,nbvals=0,workspace=workspace)
        return successful,nodes,extra,stamp,rhs




    #### Prepare stamps collection
    #### fill the hash table of the stamps
    def buildBasicStamps():
        gStamp = {}
        gStamp['V']={'name':'Générateur de tension','stampFunc':stampsMgr.voltageSource,'dispValues':('N+','N-','V_g',),'NetListDoc':'Vx N+ N- Vg','symbol':encodeFile('circuits','data','Vsource.png')}
        gStamp['I']={'name':'Générateur de courant','stampFunc':stampsMgr.currentSource,'dispValues':('N+','N-','I_g',),'NetListDoc':'Ix N+ N- Ig','symbol':encodeFile('circuits','data','Isource.png')}
        gStamp['R']={'name':'Resistance','stampFunc':stampsMgr.resistor,'dispValues':('N+','N-','R',),'NetListDoc':'Rx N+ N- R','symbol':encodeFile('circuits','data','resistor.png')}
        gStamp['L']={'name':'Bobine','stampFunc':stampsMgr.self,'dispValues':('N+','N-','L',),'NetListDoc':'Lx N+ N- L','symbol':encodeFile('circuits','data','self.png')}
        gStamp['C']={'name':'Capacité','stampFunc':stampsMgr.capacitor,'dispValues':('N+','N-','C',),'NetListDoc':'Cx N+ N- C','symbol':encodeFile('circuits','data','capacitor.png')}
        gStamp['E']={'name':'VCVS','stampFunc':stampsMgr.VCVS,'dispValues':('N+','N-','NC+','NC-','G',),'NetListDoc':'Ex N+ N- NC+ NC- G','symbol':encodeFile('circuits','data','VCVS.png')}
        gStamp['F']={'name':'CCCS','stampFunc':stampsMgr.CCCS,'dispValues':('N+','N-','NC+','NC-','G',),'NetListDoc':'Fx N+ N- NC+ NC- G','symbol':encodeFile('circuits','data','CCCS.png')}
        gStamp['G']={'name':'VCCS','stampFunc':stampsMgr.VCCS,'dispValues':('N+','N-','NC+','NC-','Y',),'NetListDoc':'Gx N+ N- NC+ NC- Y','symbol':encodeFile('circuits','data','VCCS.png')}
        gStamp['H']={'name':'CCVS','stampFunc':stampsMgr.CCVS,'dispValues':('N+','N-','NC+','NC-','T',),'NetListDoc':'Hx N+ N- NC+ NC- T','symbol':encodeFile('circuits','data','CCVS.png')}
        gStamp['X']={'name':'AOP Idéal','stampFunc':stampsMgr.IdealOpAmp,'dispValues':('S','E+','E-'),'NetListDoc':'Xx S E+ E-','symbol':encodeFile('circuits','data','AOP.png')}
        return gStamp

    ### doc creator

    def displayStampsList():

        theArray=[]
        theArray.append(["Nom et Netlist","Symbole","Inconnues","Stamp","RHS"])
        for key,stp in stampsMgr.collection.items():
            _,nodes,extra,stampMatrix,stampRHS = stp['stampFunc'](key+'x',stp['dispValues'],workspaceMgr({}))
            currentRow = []
            currentRow.append (stp['name']+'<br>  ```'+stp['NetListDoc']+'```')
            currentRow.append('<img width="150px" src="data:image/png;base64,'+stp['symbol']+'" style="width:150px !important;max-width:none"/>')
            #currentRow.append('![Vsource](Vsource.png)')
            #currentRow.append('![](data:image/svg+xml;base64,'+stp['symbol']+' =150x)')
            stampVarsList = nodes+extra
            stampVars =   sp.zeros(len(stampVarsList),1);
            for k in range(len(stampVarsList)):
                stampVars[k]=sp.Symbol(stampVarsList[k])
            #currentRow.append(sp.mathml(stampVars))
            #currentRow.append(sp.mathml(stampMatrix))
            #currentRow.append(sp.mathml(stampRHS))
            currentRow.append("$$"+sp.latex(stampVars).replace('\\','\\\\')+"$$")
            currentRow.append("$$"+sp.latex(stampMatrix).replace('\\','\\\\')+"$$")
            currentRow.append("$$"+sp.latex(stampRHS).replace('\\','\\\\')+"$$")
            theArray.append(currentRow)
        return buildMarkdownTable(theArray)



stampsMgr.collection = stampsMgr.buildBasicStamps()


##### Main sPyce operation
def sPyce(data,workspace,simplifyResult=False,verbose=True,raiseErr=False):
    """Spice pour python.
Lancez sPyceDoc() pour plus d'informations.    """


    import re  # regular expressions
    import sys
    try:
        #stampsMgr.cleanLocalSymbols()
        ws = workspaceMgr(workspace)
        #ws.buildMissingVars('s') #Laplace variable
        reader = data.splitlines()

        ###  read Netlist
        netline = iter(reader)

        ### first step : eval required voltage nodes and extra variables
        nodes = set()  # a set is a kind of list in which a given value cannot be inserted twice
        extra = set()
        stampList = []
        for k in range(0,len(reader)):
            netlineStr = next(netline).strip()
            comment = re.match(r'^(\*)',netlineStr)

            # is it a comment or empty line ?
            if(comment is None and len(netlineStr)!=0):
                # no, check if it's a device, else drop an exception
                elementsOfStr = re.split(r'[\s\t]+',netlineStr)
                if(len(elementsOfStr)<1): raise Exception('netlistSyntax',k+1)
                device = re.match(r'^([A-Za-z])([A-Za-z0-9]+)',elementsOfStr[0])
                if(len(device.groups())!=2): raise Exception('netlistSyntax',k+1)

                # call the good stamp generator for the current device
                deviceKind = device.group(1)
                if deviceKind in stampsMgr.collection :  deviceFun = stampsMgr.collection[deviceKind]['stampFunc']
                else:  raise Exception('netlistDevice',k+1,deviceKind)

                deviceName = device.group(2)
                deviceParams = elementsOfStr[1:]

                ok,localNodes,localExtra,localStamp,localRHS = deviceFun(elementsOfStr[0],deviceParams,ws)

                # update list of nodes variables and extra variables
                nodes.update(localNodes)
                extra.update(localExtra)
                stampList.append([localStamp,localRHS,localNodes,localExtra]);
        # sort nodes for better human experience
        nodes = list(nodes)
        nodes.sort()
        extra = list(extra)
        extra.sort()

        if r'V(0)'  in nodes: nodes.remove(r'V(0)')
        else: raise Exception('netlistNoGround')

        ### second step : apply MNA from all stamps
        mnaVars = list(nodes) + list(extra);
        MNA = sp.zeros(len(mnaVars),len(mnaVars));
        RHS =  sp.zeros(len(mnaVars),1)
        stamp = iter(stampList)

        for k in range(0,len(stampList)):
            # build local vars for the current stamp
            localStamp = next(stamp)
            localMNAVars = localStamp[2] + localStamp[3]
            localMNA = localStamp[0]
            localRHS = localStamp[1]

            # find and remove ground if exists
            while r'V(0)' in localMNAVars:
                idx = localMNAVars.index(r'V(0)')
                localMNAVars.pop(idx)
                localMNA.col_del(idx)
                localMNA.row_del(idx)
                localRHS.row_del(idx)

            # find index of local mna vars in main mna vars table
            idx = [mnaVars.index(var) for var in localMNAVars]

            # now fill MNA and RHS
            for i in range(localMNA.rows):
                for j in range(localMNA.cols):
                    MNA[idx[i],idx[j]] += localMNA[i,j]
            for i in range(localRHS.rows):
                RHS[idx[i],0] += localRHS[i,0]

        e = MNA.LUsolve(RHS)#MNA\RHS
        varsSolved={}
        for i in range(len(mnaVars)):
            varsSolved[mnaVars[i]]=i

        if simplifyResult is True:
            e.simplify()

        return e,varsSolved,MNA,RHS

    except IOError as err:
        if verbose==True:
            print("Error: Netlist file does not appear to exist.")
        if raiseErr==True: raise err
    except KeyError as key:
        if verbose==True:
            print("Error Line %d : The component %s does not exist." % (k+1,key))
            print (key.__dict__)
            print (sys.exc_info()[0])
        if raiseErr==True: raise key
    except NameError as ne:
        if verbose==True:
            print("Error line %d : the Symbol %s." % (k+1,ne))
        if raiseErr==True: raise ne

    #except Exception as e:
        #print "Error %s." % (e,)

    return [],{},sp.Matrix([]),sp.Matrix([])


def sPyceDoc():
    docFile = ('circuits','data','sPyceDoc.md')        
    
    credits = 'v 1.0 - par M. Myara, F. Martinez et S. Parola - Département EEA / IES UMR5214 - Université de Montpellier'
    
    imports = {'displayStampsList':stampsMgr.displayStampsList(),\
               'bottomBanner':eeaWidget(credits=credits,mode='htmlCode')}
    
    docBuilder(docFile,imports)
    