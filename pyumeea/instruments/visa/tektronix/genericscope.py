#
# This file is part of the PyEEA package.
#
# Copyright (c) 2013-2020 PyEEA Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


from pymeasure.instruments import Instrument
from pymeasure.instruments.validators import strict_discrete_set, \
    truncated_discrete_set, truncated_range,strict_range
import numpy as np
import copy

class genericTDS(Instrument):
    """ Represents a generic Tektronics oscilloscope.
    Should be compatible with lots of models, like TDS2000
    """


    CHANNEL_VALUES = {'channel1':'CH1','channel2':'CH2'}
    REF_VALUES = {'referenceA':'REFA','referenceB':'REFB'}
    MATH_VALUES = {'math':'MATH',}

    LANGUAGE_VALUES = {'english':'ENGL','french':'FREN','german':'GERM','italian':'ITAL','portuguese':'PORTU',
                        'spanish':'SPAN','japanese':'JAPA','korean':'KORE','chinese (trad)':'TRAD','chinese (simpl)':'SIMP'}
    MATHOPERATORS_VALUES = {'ch1-ch2':'CH1 - CH2','ch2-ch1':'CH2 - CH1','ch1+ch2':'CH1+CH2'}

    STATUS_VALUES = {'Off':0,'On':1}





    def __init__(self, resourceName, name="genericTDS",**kwargs):
        super(genericTDS, self).__init__(
            resourceName,name,timeout=3000,
            **kwargs
        )
        self.acquisition = genericScopeGroups.Acquisition(self)
        self.calibration = genericScopeGroups.Calibration(self)
        self.channel1 = genericScopeGroups.Channel(self,1)
        self.channel2 = genericScopeGroups.Channel(self,2)
        self.cursor  = genericScopeGroups.Cursor(self)
        self.display = genericScopeGroups.Display(self)
        self.measurementImmediate = genericScopeGroups.Measurement(self,'IMMed')
        self.measurement1 = genericScopeGroups.Measurement(self,'MEAS1')
        self.measurement2 = genericScopeGroups.Measurement(self,'MEAS2')
        self.measurement3 = genericScopeGroups.Measurement(self,'MEAS3')
        self.measurement4 = genericScopeGroups.Measurement(self,'MEAS4')
        self.measurement5 = genericScopeGroups.Measurement(self,'MEAS5')
        self.system = genericScopeGroups.System(self)
        self.trigger = genericScopeGroups.Trigger(self)
        self.waveform = genericScopeGroups.Waveform(self)


    language = Instrument.control("LANGuage?", "LANGuage %s",
                                """Scope Menu's Languge. Supported are : {}""".format(LANGUAGE_VALUES.keys()),
                                validator=strict_discrete_set,
                                values=LANGUAGE_VALUES,
                                map_values=True)

    math = Instrument.control("MATH:DEFINE?", "MATH:DEFINE %s",
                                """Math operation. Supported are : {}""".format(MATHOPERATORS_VALUES.keys()),
                                validator=strict_discrete_set,
                                values=MATH_VALUES,
                                map_values=True)

    verbose = Instrument.control("VERBose?", "VERBose %d",
                                """Verbose mode operation. Supported are : {}""".format(STATUS_VALUES.keys()),
                                validator=strict_discrete_set,
                                values=STATUS_VALUES,
                                map_values=True)

    header = Instrument.control("HEADer?", "HEADer %d",
                                """Header mode operation. Supported are : {}""".format(STATUS_VALUES.keys()),
                                validator=strict_discrete_set,
                                values=STATUS_VALUES,
                                map_values=True)

    def autoset(self):
        """Runs Autoset."""
        self.write("AUTOSet EXECUTE")

    def factoryReset(self):
        """Runs factory reset."""
        self.write("FACtory")
        self.write("HEADER OFF")

    def isBusy(self):
        response = self.ask("BUSY?")
        return int(response)

class genericScopeGroups(object):

    class Group(object):
        def __init__(self, parent,replaceList={}):
            self.parent = parent
            self.replaceList = replaceList

        def pC(self,command): #parsedCommand
            theCommand = command
            for k,v in self.replaceList.items():
                theCommand = theCommand.replace(k,v)
            return theCommand

        def ask(self, command):
            return self.parent.ask(self.pC(command))
        def write(self, command):
            self.parent.write(self.pC(command))
        def values(self, command, **kwargs):
            return self.parent.values(self.pC(command), **kwargs)
        def read(self):
            return self.parent.read()
        def binary_values(self, command, header_bytes=0, dtype=np.float32):
            return self.parent.binary_values(self.pC(command), header_bytes, dtype)
        def check_errors(self):
            return self.parent.check_errors()

    class Acquisition(Group):
        SEQUENCINGMODE_VALUES = {'run-stop':'RUNSTop','single':'SEQuence'}
        MODE_VALUES = {'sample':'SAMPLE','peakDetection':'PEAKDETECT','average':'AVERAGE'}
        AVERAGES_VALUES = [4,16,64,128]
        STATUS_VALUES = {'stop':'OFF','run':'ON'}
        VIEW_VALUES = {'main':'MAIN','window':'WINdow','zone':'ZONE'}

        def __init__(self, parent):
            super().__init__(parent)

        acquisitionAmount = Instrument.measurement('ACQuire:NUMACq?',
                    """Gets the actual amount of acquisitions.""")
        sequencingMode = Instrument.control("ACQuire:STOPAfter?", "ACQuire:STOPAfter %s",
                                    """Sets or returns whether the instrument continually acquires acquisitions or acquires a single sequence. Supported are : {}""".format(SEQUENCINGMODE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=SEQUENCINGMODE_VALUES,
                                    map_values=True)
        delay = Instrument.control("HORizontal:MAIn:POSition?", "HORizontal:MAIn:POSition %g",
                                    """Sets or queries the horizontal position.""")
        acquireMode = Instrument.control("ACQuire:MODe?", "ACQuire:MODe %s",
                                    """Sets or queries the acquisition mode of the instrument for all live waveforms.  Supported are : {}""".format(MODE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=MODE_VALUES,
                                    map_values=True)
        averagesAmount = Instrument.control("ACQuire:NUMAVg?", "ACQuire:NUMAVg %d",
                                    """Sets or queries the horizontal position.""",
                                    validator=strict_discrete_set,
                                    values=AVERAGES_VALUES)
        acquireStatus = Instrument.control("ACQ:STATE?", "ACQ:STATE %s",
                                    """Sets or queries the acquisition status.  Supported are : {}""".format(STATUS_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)
        timebase = Instrument.control("HORizontal:MAIn:SCALe?", "HORizontal:MAIn:SCALe %s",
                                    """Sets or queries the Timebase value.""" )
        viewMode = Instrument.control("HORizontal:VIEw?", "HORizontal:VIEw %s",
                                    """Sets or queries the current view mode.  Supported are : {}""".format(VIEW_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=VIEW_VALUES,
                                    map_values=True)
        windowDelay = Instrument.control("HORizontal:DELay:POSition?", "HORizontal:DELay:POSition %g",
                                    """Sets or queries the horizontal position.""")
        windowTimebase = Instrument.control("HORizontal:DELay:SCALe?", "HORizontal:DELay:SCALe %s",
                                    """Sets or queries the Timebase value.""")



    class Calibration(Group):

        STATUS_VALUES = {'fail':'FAIL','pass':'PASS'}

        def __init__(self, parent):
            super().__init__(parent)

        diagnosticResult = Instrument.measurement('DIAG:RESult:FLAg?',
                    """Get Diagnostic Result Status.""",
                    values=STATUS_VALUES,map_values=True)
        calibrationResult = Instrument.measurement('CALibrate:STATus?',
                    """Get Calibration Status.""",
                    values=STATUS_VALUES,map_values=True)
        testLog = Instrument.measurement('CALibrate:STATus?',
                    """Get Log of the tests.""")

        def abort(self):
            """Aborts Calibration.""",
            self.write('CALibrate:ABORT')
        def initialise(self):
            """Initialises Calibration.""",
            self.write('CALibrate:FACTory')
        def doContinue(self):
            """Continues Calibration.""",
            self.write('CALibrate:CONTINUE')
        def autoCalibrate(self):
            """Auto Calibration.""",
            self.write('CALibrate:INTERNAL')


    class Channel(Group):
        BWLSTATUS_VALUES = {'Off':'OFF','On':'ON'}
        CHANNEL_STATUS={'Off':0,'On':1}
        COUPLING_VALUES = {'ac':'AC','dc':'DC','ground':'GND'}
        PROBE_VALUES = [1,10,100,1000]

        def __init__(self, parent,channelNum):
            self.channelNum = channelNum
            replaceList = {'<ID>':'CH%d'%channelNum}
            super().__init__(parent,replaceList)

        bandWidthLimit = Instrument.control("<ID>:BANDwidth?", "<ID>:BANDwidth %s",
                                    """Status of the BandWithLimit low-pass filter of the current channel. Supported are : {}""".format(BWLSTATUS_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=BWLSTATUS_VALUES,
                                    map_values=True)
        coupling = Instrument.control("<ID>:COUP?", "<ID>:COUP %s",
                                    """Input signal Coupling for current channel. Supported are : {}""".format(COUPLING_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=COUPLING_VALUES,
                                    map_values=True)
        position = Instrument.control("<ID>:POSition?", "<ID>:POSition %g",
                                    """Vertical position of the signal for current channel""")
        probeFactor = Instrument.control("<ID>:PRObe?", "<ID>:PRObe %s",
                                    """Probe Factor for the current channel. Supported are : {}""".format(PROBE_VALUES),
                                    validator=strict_discrete_set,
                                    values=PROBE_VALUES,
                                    map_values=True)
        scale = Instrument.control("<ID>:SCAle?", "<ID>:SCAle %g",
                                    """Scale factor for the current channel""")
        status = Instrument.control("Select:<ID>?", "Select:<ID> %d",
                                    """Activated or activated channel. Supported are : {}""".format(CHANNEL_STATUS),
                                    validator=strict_discrete_set,
                                    values=CHANNEL_STATUS,
                                    map_values=True)
    class Cursor(Group):

        CURSORTYPE_VALUES= {'horizontalBars':'HBArs','verticalBars':'VBArs','none':'OFF'}
        HBARUNIT_VALUES = {'volt':'VOL','division':'DIV','decibel':'DECIBEL','unknown':'UNKNOWN'}
        VBARUNIT_VALUES = {'second':'SECO','hertz':'HER'}



        def __init__(self, parent):
            super().__init__(parent)

        type = Instrument.control("CURSor:FUNCtion?", "CURSor:FUNCtion %s",
                                    """Kind of actual cursor. Supported are : {}""".format(CURSORTYPE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=CURSORTYPE_VALUES,
                                    map_values=True)
        def horizontalBarsDelta(self):
            """Distance between horizontal bar cursors"""
            return float(self.parent.ask('CURSor:HBArs:DELTa?'))

        horizontalBarUnits = Instrument.control("CURSor:HBARS:UNIts?", "CURSor:HBARS:UNIts %s",
                                    """Physical unit for horizontal bar cursors. Supported are : {}""".format(HBARUNIT_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=HBARUNIT_VALUES,
                                    map_values=True)
        horizontalBarPosition1 = Instrument.control("CURSor:HBArs:POSITION1?", "CURSor:HBArs:POSITION1 %s",
                                    """Horizontal Cursor 1 value""",)
        horizontalBarPosition2 = Instrument.control("CURSor:HBArs:POSITION2?", "CURSor:HBArs:POSITION2 %s",
                                    """Horizontal Cursor 2 value""",)
        source = Instrument.control("CURSor:SELect:SOUrce?", "CURSor:SELect:SOUrce %s",
                                    """Source for cursor operation. Supported are : {}""".format(CURSORTYPE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=CURSORTYPE_VALUES,
                                    map_values=True)

        def verticalBarsDelta(self):
            """Distance between vertical bar cursors"""
            return float(self.parent.ask('CURSor:VBArs:DELTa?'))
        verticalBarUnits = Instrument.control("CURSor:HBARS:UNIts?", "CURSor:HBARS:UNIts %s",
                                    """Physical unit for vertical bar cursors. Supported are : {}""".format(HBARUNIT_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=HBARUNIT_VALUES,
                                    map_values=True)
        verticalBarPosition1 = Instrument.control("CURSor:VBArs:POSITION1?", "CURSor:VBArs:POSITION1 %s",
                                    """Vertical Cursor 1 value""",)
        verticalBarPosition2 = Instrument.control("CURSor:VBArs:POSITION2?", "CURSor:VBArs:POSITION2 %s",
                                    """Vertical Cursor 2 value""",)



    class Display(Group):

        FORMAT_VALUES = {'xy':'XY','yt':'YT'}
        PERSISTENCE_VALUES = [0,1,2,5,99]
        STYLE_VALUES = {'dots':'DOTs','vectors':'VECtors'}

        def __init__(self, parent):
            super().__init__(parent)

        contrast = Instrument.control("DISPlay:CONTRast?", "DISPlay:CONTRast %g",
                                    """Screen contrast. Range is 1-100""",
                                    validator=strict_range,
                                    values=(1,100))

        format = Instrument.control("DISPlay:FORMat?", "DISPlay:FORMat %s",
                                    """Display Format. Supported are : {}""".format(FORMAT_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=FORMAT_VALUES,
                                    map_values=True)
        persistence = Instrument.control("DIS:PERS?", "DIS:PERS %s",
                                    """Display Persistence. Supported are : {}""".format(PERSISTENCE_VALUES),
                                    validator=strict_discrete_set,
                                    values=PERSISTENCE_VALUES,
                                    map_values=True)
        style = Instrument.control("DISPlay:STYle?", "DISPlay:STYle %s",
                                    """Display Style. Supported are : {}""".format(STYLE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=STYLE_VALUES,
                                    map_values=True)


    class Measurement(Group):
        SOURCE_VALUES = {**genericTDS.CHANNEL_VALUES,**genericTDS.MATH_VALUES}

        TYPE_VALUES = {
            'frequency':'FREQuency','max':'MAXImum','mean':'MEAN','min':'MINImum',
            'peakToPeak':'PK2pk','rms':'CRMs','riseTime':'RISe','fallTime':'FALL',
            'negativeWidth':'NWIdth','positiveWidth':'PWIdth','none':'NONE'
            }


        #UNIT_VALUES = {'volt':'V', 'second':'s', 'hertz':'Hz'}

        def __init__(self, parent,ID):

            super().__init__(parent,{'<ID>':'MEASU:%s:'%ID})

        value = Instrument.measurement('<ID>VAL?',
                    """Gets actual value.""")
        kind = Instrument.control("<ID>TYPE?", "<ID>TYPE %s",
                                    """Defines measurement kind. Supported are : {}""".format(TYPE_VALUES),
                                    validator=strict_discrete_set,
                                    values=TYPE_VALUES,
                                    map_values=True)
        source1 = Instrument.control("<ID>SOUrce1?", "<ID>source1 %s",
                                    """Defines the source 1 for the measurement. Supported are : {}""".format(SOURCE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=SOURCE_VALUES,
                                    map_values=True)
        source2 = Instrument.control("<ID>SOUrce2?", "<ID>source2 %s",
                                    """Defines the source 2 for the measurement. Supported are : {}""".format(SOURCE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=SOURCE_VALUES,
                                    map_values=True)

        unit = Instrument.measurement('<ID>UNIts?',
                    """Gets current measurement physical unit."""
                    )


    class System(Group):

        LOCK_VALUES = {'disable':'ALL','enable':'NONE'}

        def __init__(self, parent):
            super().__init__(parent)

        def localControls(self,value):
            """Enable or disable scope controls. Values are in {}""".format(self.LOCK_VALUES.keys())
            if value not in self.LOCK_VALUES.keys():
                raise ValueError('Invalid mode. Modes are in {}'.format(self.LOCK_VALUES.keys()))
            self.parent.write("LOCK "+self.LOCK_VALUES[value])

        def loadState(self,value):
            """load configuration status from given memory bank. Value is in 1-10"""
            if value not in range(1,11):
                raise ValueError('Invalid value. Value is in 1-10')
            self.parent.write("*RCL %d"%value)

        def saveState(self,value):
            """save configuration status from given memory bank. Value is in 1-10"""
            if value not in range(1,11):
                raise ValueError('Invalid value. Value is in 1-10')
            self.parent.write("*SAV %d"%value)

    class Trigger(Group):

        SOURCE_VALUES = {**genericTDS.CHANNEL_VALUES,**{'ext':'EXT','ext5':'EXT5','line':'LINE'}}
        VIDEOSOURCE_VALUES = {**genericTDS.CHANNEL_VALUES,**{'ext':'EXT','ext5':'EXT5'}}
        VIDEOSYNC_VALUES = {'field':'field','line':'line'}

        COUPLING_VALUES = {'ac':'AC','dc':'DC','highFrequencyRejection':'HFREJ',
                           'lowFrequencyRejection':'LFREJ','noiseRejection':'NOISEREJ'}
        MODE_VALUES = {'auto':'AUTO','normal':'NORMAL'}
        SLOPE_VALUES={'falling':'FALL','rising':'RISE'}
        STATUS_VALUES={'armed':'ARMED','ready':'READY','trigger':'TRIGGER','auto':'AUTO','save':'SAVE','scan':'SCAN'}
        TYPE_VAlUES={'edge':'EDGE','video':'VID','pulse':'PUL'}
        POLARITY_VALUES={'nomrmal':'NORMAL','inverted':'INVERT'}


        def __init__(self, parent):
            super().__init__(parent)

        coupling = Instrument.control("DATa:STARt?", "DATa:STARt %s",
                                    """Trigger Coupling. Supported are : {}""".format(COUPLING_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=COUPLING_VALUES,
                                    map_values=True)
        holdoff = Instrument.control("Trigger:Main:Holdoff?", "Trigger:Main:Holdoff:Value %g",
                                    """Trigger Hold off value""")
        level = Instrument.control("TRIGGer:MAIN:LEVel?", "TRIGGer:MAIN:LEVel  %g",
                                    """Trigger level""")
        mode = Instrument.control("Trigger:Main:Mode?", "Trigger:Main:Mode %s",
                                    """Trigger Mode. Supported are : {}""".format(MODE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=MODE_VALUES,
                                    map_values=True)
        slope = Instrument.control("Trigger:Main:Edge:Slope?", "Trigger:Main:Edge:Slope %s",
                                    """Trigger Mode. Supported are : {}""".format(SLOPE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=SLOPE_VALUES,
                                    map_values=True)
        source = Instrument.control("Trigger:Main:Edge:Source?", "Trigger:Main:Edge:Source %s",
                                    """Source for trigger. Supported are : {}""".format(SOURCE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=SOURCE_VALUES,
                                    map_values=True
                                    )
        type = Instrument.control("Trigger:Main:Type?", "Trigger:Main:Type %s",
                                    """Trigger Type. Supported are : {}""".format(TYPE_VAlUES.keys()),
                                    validator=strict_discrete_set,
                                    values=TYPE_VAlUES,
                                    map_values=True)
        polarity = Instrument.control("Trigger:Main:Video:Polarity?", "Trigger:Main:Video:Polarity %s",
                                    """Trigger Polarity. Supported are : {}""".format(POLARITY_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=POLARITY_VALUES,
                                    map_values=True)
        videoSource = Instrument.control("Trigger:Main:Video:Source?", "Trigger:Main:Video:Source %s",
                                    """VideoSource for trigger. Supported are : {}""".format(VIDEOSOURCE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=VIDEOSOURCE_VALUES,
                                    map_values=True)
        videoSync = Instrument.control("Trigger:Main:Video:Sync?", "Trigger:Main:Video:Sync %s",
                                    """Source for trigger. Supported are : {}""".format(VIDEOSYNC_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=VIDEOSYNC_VALUES,
                                    map_values=True)

        def state(self):
            """Gets trigger status"""
            return STATUS_VALUES[self.parent.ask('TRIGger:STATE?')]
        def triggerNow(self):
            """Force trigger Now"""
            self.parent.write("*TRG")



    class Waveform(Group):
        SOURCE_VALUES = {**genericTDS.CHANNEL_VALUES,**genericTDS.REF_VALUES,**genericTDS.MATH_VALUES}

        DATAENCODING_VALUES = {'ascii':('ASCII',1),  'int8':('RIBINARY',1), 'uint8':('RPBINARY',1),
                           'int16_little':('RIBINARY',2), 'uint16_little':('RPBINARY',2),
                           'int16_big':('SRIBINARY',2),  'uint16_big':('SRPBINARY',2)
                           }

        def __init__(self, parent):
            super().__init__(parent)


        firstPoint = Instrument.control("DATa:STARt?", "DATa:STARt %g",
                                    """First point to get in actual trace.""")
        lastPoint = Instrument.control("DATa:STOP?", "DATa:STOP %g",
                                    """Last point to get in actual trace.""")
        @property
        def encoding(self):
            """Encoding of curves traces"""
            encodingKind = self.ask('DATA:ENCdg?').strip()
            encodingWidth = self.ask('DATA:WIDTH?').strip()
            for (key,value) in self.DATAENCODING_VALUES.items():
                if encodingKind == value[0] and int(encodingWidth) == value[1]:
                    return key
            raise ValueError("Invalid data format.")

        @encoding.setter
        def encoding(self,value):
            if value not in self.DATAENCODING_VALUES.keys():
                raise ValueError("Invalid data format.")

            toSend = self.DATAENCODING_VALUES[value]
            self.write('DATA:ENCdg '+ toSend[0])
            self.write('DATA:WIDTH '+ str(toSend[1]))


        def downloadRaw(self,source,dtype=np.int16):
            """Downloads trace from given Channel. Raw 8 (np.int8) or 16 bit (np.int16)
            signed format."""
            saved_encoding = self.encoding
            if dtype == np.int8:
                self.encoding = 'int8'
            elif dtype == np.int16:
                self.encoding = 'int16_big'
            else:
                ValueError('Wanted encoding not supported !')

            self.write('DATA:SOURCE ' + source);
            raw = self.binary_values('CURVE?',header_bytes=0, dtype=np.uint8);

            if raw[0]!=b'#'[0] :
                raise ValueError("Incorrect data format during Visa exchange with source ('%s') provided to %s" % (
                                 self.parent, source))
            sizeDigits = int(raw[1].tostring().decode("ascii"))
            size = int(raw[2:2+sizeDigits].tostring().decode("ascii"))
            dataStart = 2+sizeDigits
            dataEnd = dataStart + size

            data = np.frombuffer(raw[dataStart:dataEnd].tostring(), dtype=dtype)

            self.encoding = saved_encoding
            return data

        def calY(self,data):
            yoffs = float(self.ask('WFMPRE:YOFF?'))
            ymult = float(self.ask('WFMPRE:YMULT?'))
            yzero = float(self.ask('WFMPRE:YZERO?'))
            y = ((data - yoffs) * ymult) + yzero
            return y

        def buildX(self):
            xzero = float(self.ask('WFMPRE:XZERO?'));
            xincr = float(self.ask('WFMPRE:XINCR?'));
            ptcnt = float(self.ask('WFMPRE:NR_PT?'));
            x = np.arange(0,ptcnt,1) *xincr + xzero
            return x

        def checkSource(self,source):
            if source not in self.SOURCE_VALUES.keys():
                raise ValueError("Invalid source ('%s') provided to %s" % (
                                 self.parent, value))
            return self.SOURCE_VALUES[source]

        def download(self,source,getUnits=False):
            """Downloads trace from given Channel."""
            data = self.downloadRaw( self.checkSource(source))
            y = self.calY(data)
            x  =  self.buildX()
            result = [x,y]

            if getUnits==True:
                xunit = self.ask('WFMPRE:XUnit?').strip().replace('"','');
                yunit = self.ask('WFMPRE:YUnit?').strip().replace('"','');
                result = result + [xunit,yunit]

            return result


        def store(self,source,dest):
            """Stores trace from Channel to Ref A or B."""
            if source not in genericTDS.CHANNEL_VALUES:
                raise ValueError("Invalid source ('%s') provided to %s" % (
                                 self.parent, source))
            if dest not in genericTDS.REF_VALUES:
                raise ValueError("Invalid destination ('%s') provided to %s" % (
                                 self.parent, dest))

            self.parent.write('SAVe:WAVEform '+source+', '+dest)

        def upload(self,data,dest):
            """Uploads 16 bit local trace in REF A or B."""
            if dest not in genericTDS.REF_VALUES:
                raise ValueError("Invalid destination ('%s') provided to %s" % (
                                 self.parent, source))

            saved_encoding = self.encoding
            self.encoding = 'int16_big'

            self.parent.write('DATA:DESTination '+dest)
            header = ('CURVe #%d%d'%(sizeDigits,theSize)).encode('ascii')
            footer = ('\n').encode('ascii')
            message = np.concatenate([header,np.frombuffer(data, dtype=np.int8),footer])
            self.parent.write(message)

            self.encoding = saved_encoding

        @property
        def pointsAmount(self):
            """Amount of memory points for a channel"""
            return int(self.parent.ask('HORizontal:RECordlength?'))



class genericTDS4Channels(genericTDS):
    MATHOPERATORS_VALUES = {'ch1+ch2':'CH1+CH2','ch3+ch4':'CH3+CH4',
    'ch1-ch2':'CH1-CH2','ch2-ch1':'CH2-CH1','ch3-ch4':'CH3-CH4'}
    CHANNEL_VALUES = ['CH1','CH2','CH3','CH4']
    REF_VALUES = ['REFA','REFB','REFC','REFD']


    def __init__(self, resourceName,name="Tektronix TDS 4 Channels", **kwargs):
        super(genericTDS4Channel, self).__init__(
            resourceName,**kwargs
        )
        self.channel3 = genericScopeGroups.Channel(self,3)
        self.channel4 = genericScopeGroups.Channel(self,4)
