#
# This file is part of the PyEEA package.
#
# Copyright (c) 2013-2020 PyEEA Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

from .genericscope import *


class genericTBS1000ScopeGroups(genericScopeGroups):
    class FFT(genericScopeGroups.Group):
        displayState = Instrument.control("SELECT:FFT?", "SELECT:FFT %s",
                                    """Status of the FFT. Supported are : {}""".format(genericTDS.STATUS_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=genericTDS.STATUS_VALUES,
                                    map_values=True)
        source = Instrument.control("FFT:SOURce?", "FFT:SOURce %s",
                                    """FFT Source. Supported are : {}""".format(genericTDS.CHANNEL_VALUES),
                                    validator=strict_discrete_set,
                                    values=genericTDS.CHANNEL_VALUES,
                                    map_values=True)

    class WaveformTBS1000(genericScopeGroups.Waveform):
        SOURCE_VALUES ={**genericTDS.CHANNEL_VALUES,**genericTDS.REF_VALUES,**{'math':'FFT'}}

        def download(self,source,getUnits=False):
            if self.parent.fft.displayState == 'On':
                source='math'
            return super(genericTBS1000ScopeGroups.WaveformTBS1000, self).download(source,getUnits)

    class MeasurementTBS1000(genericScopeGroups.Measurement):
        TYPE_VALUES = {
                'amplitude':'AMplitude', 'area':'AREA','burst':'BURst',
                'cycleArea':'CAREA','cycleMean':'CMEAN', 'CycleRMS':'CRMs',
                'delay':'DELay','fallTime':'FALL','frequency':'FREQuency',
                'high':'HIGH','low':'LOW','max':'MAXImum','mean':'MEAN','min':'MINImum',
                'negativeDuty':'NDUty','negativeEdgesCount':'NEDGECount','negativeOverShoot':'NOVERshoot',
                'negativePulseCount':'NPULSECount','negativeWidth':'NWIdth',
                'positiveDuty':'PDUty','positiveEdgesCount':'PEDGECount','positiveOverShoot':'POVERshoot',
                'positivePulseCount':'PPULSECount','positiveWidth':'PWIdth',
                'phase':'PHAse','peakToPeak':'PK2pk','riseTime':'RISe','rms':'RMS','none':'NONE'
                }

### surprizing specific operating mode for this scope ...
class genericTBS1000(genericTDS):
    def __init__(self, resourceName,name="Tektronix generic TBS 1000", **kwargs):
        super(genericTBS1000, self).__init__(
            resourceName,name,**kwargs
        )
        self.fft = genericTBS1000ScopeGroups.FFT(self)
        del self.waveform
        self.waveform = genericTBS1000ScopeGroups.WaveformTBS1000(self)

        del self.measurementImmediate ,self.measurement1,self.measurement2,self.measurement3,self.measurement4,self.measurement5
        self.measurement1 = genericTBS1000ScopeGroups.MeasurementTBS1000(self,'MEAS1')
        self.measurement2 = genericTBS1000ScopeGroups.MeasurementTBS1000(self,'MEAS2')
        self.measurement3 = genericTBS1000ScopeGroups.MeasurementTBS1000(self,'MEAS3')
        self.measurement4 = genericTBS1000ScopeGroups.MeasurementTBS1000(self,'MEAS4')
        self.measurement5 = genericTBS1000ScopeGroups.MeasurementTBS1000(self,'MEAS5')




class TBS1072(genericTBS1000):
    """ Represents a  Tektronics TBS 1072 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TBS1202, self).__init__(
            resourceName,
            "Tektronix TBS 1072",**kwargs
        )

class TBS1102(genericTBS1000):
    """ Represents a  Tektronics TBS 1102 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TBS1202, self).__init__(
            resourceName,
            "Tektronix TBS 1102",**kwargs
        )

class TBS1152(genericTBS1000):
    """ Represents a  Tektronics TBS 1152 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TBS1202, self).__init__(
            resourceName,
            "Tektronix TBS 1152",**kwargs
        )


class TBS1154(genericTBS1000):
    """ Represents a  Tektronics TBS 1154 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TBS1202, self).__init__(
            resourceName,
            "Tektronix TBS 1154",**kwargs
        )

class TBS1202(genericTBS1000):
    """ Represents a  Tektronics TBS 1202 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TBS1202, self).__init__(
            resourceName,
            "Tektronix TBS 1202",**kwargs
        )
