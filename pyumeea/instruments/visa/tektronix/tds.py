#
# This file is part of the PyEEA package.
#
# Copyright (c) 2013-2020 PyEEA Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

from .genericscope import *

class TDS1002(genericTDS):
    """ Represents a  Tektronics TDS 1002 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TDS1002, self).__init__(
            resourceName,
            "Tektronix TDS 1002",**kwargs
        )

class TDS1012(genericTDS):
    """ Represents a  Tektronics TDS 1012 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TDS1012, self).__init__(
            resourceName,
            "Tektronix TDS 1012",**kwargs
        )


class TDS2012(genericTDS):
    """ Represents a  Tektronics TDS 2012 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TDS2012, self).__init__(
            resourceName,
            "Tektronix TDS2012",**kwargs
        )

class TDS2022(genericTDS):
    """ Represents a  Tektronics TDS 2022 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TDS2022, self).__init__(
            resourceName,
            "Tektronix TDS 2022",**kwargs
        )
class TDS210(genericTDS):
    """ Represents a  Tektronics TDS 210 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TDS210, self).__init__(
            resourceName,
            "Tektronix TDS 210",**kwargs
        )

class TDS220(genericTDS):
    """ Represents a  Tektronics TDS 220 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TDS220, self).__init__(
            resourceName,
            "Tektronix TDS 220",**kwargs
        )

class TPS1012(genericTDS):
    """ Represents a  Tektronics TPS 1012 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TPS1012, self).__init__(
            resourceName,
            "Tektronix TPS 1012",**kwargs
        )

class TDS2014(genericTDS4Channels):
    """ Represents a  Tektronics TDS 2014 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TDS2014, self).__init__(
            resourceName,
            "Tektronix TDS 2014",**kwargs
        )

class TDS2024(genericTDS4Channels):
    """ Represents a  Tektronics TDS 2024 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(TDS2024, self).__init__(
            resourceName,
            "Tektronix TDS 2024",**kwargs
        )
