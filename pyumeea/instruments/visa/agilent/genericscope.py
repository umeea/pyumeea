
from pymeasure.instruments import Instrument
from pymeasure.instruments.validators import strict_discrete_set, \
    truncated_discrete_set, truncated_range,strict_range
import numpy as np
import copy
import time

class genericDSO(Instrument):
    """ Represents a generic Agilent oscilloscope.
    Should be compatible with lots of models, like DSO-X 2002A
    """

    CHANNEL_VALUES ={'channel1':'CHAN1','channel2':'CHAN2'}
    FUNCTION_VALUES = {'function':'FUNC'}
    SBUS_VALUES = {'serialBus1':'SBUS1'}
    MATH_VALUES={'math':'MATH'}


    STATUS_VALUES = {'off':0,'on':1}

    def __init__(self, resourceName, name="genericDSO",**kwargs):
        super(genericDSO, self).__init__(
            resourceName,name,timeout=3000,
            **kwargs
        )
        self.channel1 = genericAgilentScopeGroups.Channel(self,1)
        self.channel2 = genericAgilentScopeGroups.Channel(self,2)
        self.timebase = genericAgilentScopeGroups.Timebase(self)
        self.waveform = genericAgilentScopeGroups.Waveform(self)
        self.trigger = genericAgilentScopeGroups.Trigger(self)
        self.math = genericAgilentScopeGroups.Math(self)

    def autoset(self):
        """Runs Autoset."""
        self.write(":AUTOSCALE")

    def factoryReset(self):
        """Runs factory reset."""
        self.write(":SYSTem:PRESet")

    def run(self):
        self.write(":RUN")
    def single(self):
        self.write(":SINGle")
    def stop(self):
        self.write(":STOP")


    STATUS_VALUES = {'off':'OFF','on':'ON'}
    lockPanel = Instrument.control(":SYSTem:LOCK?", ":SYSTem:LOCK %s",
                                """Locks or Unlocks the scope front panel. Possible values are : {}""".format(STATUS_VALUES.keys()),
                                validator=strict_discrete_set,
                                values=STATUS_VALUES,
                                map_values=True)


class genericAgilentScopeGroups(object):

    class Group(object):
        def __init__(self, parent,replaceList={}):
            self.parent = parent
            self.replaceList = replaceList

        def pC(self,command): #parsedCommand
            theCommand = command
            for k,v in self.replaceList.items():
                theCommand = theCommand.replace(k,v)
            return theCommand

        def ask(self, command):
            return self.parent.ask(self.pC(command))
        def write(self, command):
            self.parent.write(self.pC(command))
        def values(self, command, **kwargs):
            return self.parent.values(self.pC(command), **kwargs)
        def read(self):
            return self.parent.read()
        def binary_values(self, command, header_bytes=0, dtype=np.float32):
            return self.parent.binary_values(self.pC(command), header_bytes, dtype)
        def check_errors(self):
            return self.parent.check_errors()


    class Channel(Group):
        STATUS_VALUES = {'off':0,'on':1}
        COUPLING_VALUES = {'ac':'AC','dc':'DC'}
        IMPEDANCE_VALUES  ={'1MOhm':'ONEM'}
        PROBE_VALUES  ={'0.001X':'0.001X','0.01X':'0.01X','0.1X':'0.1X','1X':'1X','10X':'10X','100X':'100X','1000X':'1000X'}

        def __init__(self, parent,channelNum):
            self.channelNum = channelNum
            replaceList = {'<ID>':':CHANnel%d'%channelNum}
            super().__init__(parent,replaceList)

        bandWidthLimit = Instrument.control("<ID>:BWLimit?", "<ID>:BWLimit %s",
                                    """BandWithLimit low-pass filter of the current channel. Possible values are : {}""".format(STATUS_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)
        coupling = Instrument.control("<ID>:COUPling?", "<ID>:COUPling %s",
                                    """Input signal Coupling for current channel. Supported are : {}""".format(COUPLING_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=COUPLING_VALUES,
                                    map_values=True)
        display = Instrument.control("<ID>:DISPlay?", "<ID>:DISPlay %s",
                                    """Display given channel. Possible values are : {}""".format(STATUS_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)
        impedance = Instrument.control("<ID>:IMPedance?", "<ID>:IMPedance %s",
                                    """Impedance for given channel. Possible values are : {}""".format(IMPEDANCE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)
        invert = Instrument.control("<ID>:INVert?", "<ID>:INVert %s",
                                    """Invert mode for given channel. Possible values are : {}""".format(STATUS_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)
        offset = Instrument.control("<ID>:OFFSet?", "<ID>:OFFSet %s",
                                    """offset value  for given channel in Volts""")
        position = offset

        probeFactor = Instrument.control("<ID>:PRObe?", "<ID>:PRObe %s",
                                    """Probe Factor for the current channel.""")
        range = Instrument.control("<ID>:RANGe?", "<ID>:RANGe %g",
                                    """Scale factor for the current channel""")

        scale = Instrument.control("<ID>:SCAle?", "<ID>:SCAle %g",
                                    """Scale factor for the current channel""")
        value = scale


    class Timebase(Group):
        def __init__(self, parent):
            replaceList = {'#':':TIMEbase'}
            super().__init__(parent,replaceList)

        MODE_VALUES = {'main':'MAIN','window':'WIND','xy':'XY','roll':'ROLL'}
        REF_VALUES = {'left':'LEFT','center':'CENT','right':'RIGH'}


        mode = Instrument.control("#:MODE?", "#:MODE %s",
                                    """Timebase Mode. Possible values are : {}""".format(MODE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=MODE_VALUES,
                                    map_values=True)
        position = Instrument.control("#:POSition?", "#:POSition %s",
                                    """time from the trigger event to the display reference point .""")
        range = Instrument.control("#:RANGe?", "#:RANGe %s",
                                    """time for 10 div in seconds.""")
        reference = Instrument.control("#:REFerence?", "#:REFerence %s",
                                    """Timebase reference. Possible values are : {}""".format(REF_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=REF_VALUES,
                                    map_values=True)
        scale =   Instrument.control("#:SCALe?", "#:SCALe %s",
                                    """time/div in seconds.""")
        value = scale

        windowPosition = Instrument.control("#:WINDow:POSition?", "#:WINDow:POSition %s",
                                    """time from the trigger event to the zoomed view reference point.""")
        windowRange = Instrument.control("#:WINDow:RANGe?", "#:WINDow:RANGe %s",
                                    """range value in seconds format for the zoomed window.""")
        windowScale = Instrument.control("#:WINDow:SCALe?", "#:WINDow:SCALe %s",
                                    """scale value in seconds format for the zoomed window.""")


    class Trigger(Group):
        def status(self):
            status = int(self.parent.ask(":OPERegister:CONDition?").strip(' \n\r'))
            #print(status)
            if status & (2**3) == 2**3:
                if status & (2**5)==2**5:
                    return 'WAIT'
                else:  return 'RUN'
            else: 
                return 'STOP'
            return status

        def waitStopStatus(self):
            if  self.status() == 'STOP':
                return
            while self.status() != 'STOP':
                #print(self.status())
                time.sleep(0.1)

    class Math(Group):
        def __init__(self, parent):
            replaceList = {'#':':MATH'}
            super().__init__(parent,replaceList)

        operation =   Instrument.control("#:OPERate?", "#:OPERate %s",
                                    """current Math operation.""")


    class Waveform(Group):
        SOURCE_VALUES = {**genericDSO.CHANNEL_VALUES,**genericDSO.FUNCTION_VALUES,**genericDSO.SBUS_VALUES,**genericDSO.MATH_VALUES}

        DATAENCODING_VALUES = {'ascii':('MSBF','ASCII',1),  'int8':('MSBF','BYTE',0), 'uint8':('MSBF','BYTE',1),
                           'int16_little':('MSBF','WORD',0), 'uint16_little':('MSBF','WORD',1),
                           'int16_big':('LSBF','WORD',0),  'uint16_big':('LSBF','WORD',1)
                           }
        POINTSMODE_VALUES = {'normal':'NORM','maximum':'MAX','raw':'RAW'}

        def __init__(self, parent):
            super().__init__(parent)

        pointsAmount = Instrument.control(":WAVeform:POINts?", ":WAVeform:POINts %s",
                                    """Amount of points to download.""")
        pointsAmountMode = Instrument.control(":WAVeform:POINts:MODE?", ":WAVeform:POINts:MODE %s",
                                    """Mode of points to download. Possible values are : {}""".format(POINTSMODE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=POINTSMODE_VALUES,
                                    map_values=True)
        @property
        def encoding(self):
            """Encoding of curves traces"""
            encodingEndianness = self.ask(':WAVeform:BYTeorder?').strip()
            encodingFormat = self.ask(':WAVeform:FORMat?').strip()
            encodingUnsignedness = int(self.ask(':WAVeform:UNSigned?').strip())
            if encodingFormat=='ASCII':
                return 'ascii'
            else:
                for (key,value) in self.DATAENCODING_VALUES.items():
                    if encodingEndianness == value[0] and encodingFormat == value[1] and encodingUnsignedness == value[2]:
                        return key
            raise ValueError("Invalid data format.")

        @encoding.setter
        def encoding(self,value):
            if value not in self.DATAENCODING_VALUES.keys():
                raise ValueError("Invalid data format.")

            toSend = self.DATAENCODING_VALUES[value]
            self.write(':WAVeform:BYTeorder '+ toSend[0])
            self.write(':WAVeform:FORMat '+ toSend[1])
            self.write(':WAVeform:UNSigned '+ str(toSend[2]))



        def downloadRaw(self,source,dtype=np.uint8):
            """Downloads trace from given Channel. Raw 8 (np.int8) or 16 bit (np.int16)
            signed format."""
            saved_encoding = self.encoding
            if dtype == np.int8:
                self.encoding = 'int8'
            elif dtype == np.int16:
                self.encoding = 'int16_big'
            else:
                ValueError('Wanted encoding not supported !')

            self.write(':WAVeform:SOURce ' + source);
            raw = self.binary_values(':WAVeform:DATA?',header_bytes=0, dtype=np.uint8);

            if raw[0]!=b'#'[0] :
                raise ValueError("Incorrect data format during Visa exchange with source ('%s') provided to %s" % (
                                 self.parent, source))
            sizeDigits = int(raw[1].tostring().decode("ascii"))
            size = int(raw[2:2+sizeDigits].tostring().decode("ascii"))
            dataStart = 2+sizeDigits
            dataEnd = dataStart + size

            data = np.frombuffer(raw[dataStart:dataEnd].tostring(), dtype=dtype)

            self.encoding = saved_encoding
            return data

        # I guess there is a bug with this scope in FFT mode when other mode than uint8
        # is enabled : YREFerence YINCrement and YORigin seem not to be dependent
        # on raw encoding configuration in FFT mode.
        def calY(self,data):
            yoffs = float(self.ask(':WAVeform:YREFerence?'))
            ymult = float(self.ask(':WAVeform:YINCrement?'))
            yzero = float(self.ask(':WAVeform:YORigin?'))
            y = ((yoffs-data) * ymult) 
            #y = ((yoffs-data) * ymult) + yzero
            return y

        def buildX(self,source,forcePointsAmount=0):
            xzero = float(self.ask(':WAVeform:XORigin?'))
            xincr = float(self.ask(':WAVeform:XINCrement?'))
            xzero = float(self.ask(':WAVeform:XREFerence?'))
            if forcePointsAmount == 0:
                ptcnt = float(self.pointsAmount)#float(self.ask('WFMPRE:NR_PT?'))
            else:
                ptcnt = forcePointsAmount
            x = np.arange(0,ptcnt,1) *xincr + xzero
            return x

        def checkSource(self,source):
            if source not in self.SOURCE_VALUES.keys():
                raise ValueError("Invalid source ('%s') provided to %s" % (
                                 self.parent, value))
            return self.SOURCE_VALUES[source]

        def download(self,source):
            """Downloads trace from given Channel."""
            data = self.downloadRaw(self.checkSource(source))
            y = self.calY(data)
            self.write(':WAVeform:SOURce ' + source);
            x  =  self.buildX(source,len(data))
            result = [x,y]

            return result
