from .genericscope import *

class DSOX2002(genericDSO):
    """ Represents an Agilent DSO-X 2002 oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(DSOX2002, self).__init__(
            resourceName,
            "Agilent DSO-X 2002",**kwargs
        )
class EDUX1002G(genericDSO):
    """ Represents a Keysight EDUX 1002G oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(EDUX1002G, self).__init__(
            resourceName,
            "Keysight Technologies EDU-X 1002",**kwargs
        )


class DSO1002A(genericDSO):
    """ Represents an Agilent DSO DSO1002A oscilloscope.
    """
    def __init__(self, resourceName, **kwargs):
        super(DSO1002A, self).__init__(
            resourceName,
            "Agilent DSO 1002", write_termination = '\n',red_termination='\n',**kwargs
        )
        self.waveform = DSO1002A.WaveformSelf(self)
        self.timebase = DSO1002A.TimeBaseSelf(self)
        self.trigger =  DSO1002A.TriggerSelf(self)


    STATUS_VALUES_ = {'off':'DISable','on':'ENABle'}
    lockPanel = Instrument.control(":KEY:LOCK?", ":KEY:LOCK %s",
                                """Locks or Unlocks the scope front panel. Possible values are : {}""".format(STATUS_VALUES_.keys()),
                                validator=strict_discrete_set,
                                values=STATUS_VALUES_,
                                map_values=True)

    class TriggerSelf(genericAgilentScopeGroups.Group):
        def status(self):
            return (self.parent.ask('TRIGger:STATus?').strip(' \n\r'))

        def waitStopStatus(self):
            while self.status() != 'STOP':
                time.sleep(0.01)

    class TimeBaseSelf(genericAgilentScopeGroups.Group):
        def __init__(self, parent):
            replaceList = {'#':':TIMEbase'}
            super().__init__(parent,replaceList)

        MODE_VALUES = {'main':'MAIN','delayed':'DELayed'}


        mode = Instrument.control("#:MODE?", "#:MODE %s",
                                    """Timebase Mode. Possible values are : {}""".format(MODE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=MODE_VALUES,
                                    map_values=True)
        offset = Instrument.control("#:OFFSet?", "#:OFFSet %s",
                                    """time from the trigger event to the display reference point .""")
        scale =   Instrument.control("#:SCALe?", "#:SCALe %s",
                                    """time/div in seconds.""")
        delayedOffset = Instrument.control("#:DELayed:OFFSet?", "#:DELayed:OFFSet %s",
                                    """time from the trigger event to the display reference point .""")
        delayedScale =   Instrument.control("#:DELayed:SCALe?", "#:DELayed:SCALe %s",
                                    """time/div in seconds.""")
        value = scale




    class WaveformSelf(genericAgilentScopeGroups.Waveform):

        @property
        def encoding(self):
            """Encoding of curves traces"""
            encodingEndianness = 'MSBF'#self.ask(':WAVeform:BYTeorder?').strip()
            encodingFormat = self.ask(':WAVeform:FORMat?').strip()
            encodingUnsignedness = 1#int(self.ask(':WAVeform:UNSigned?').strip())
            if encodingFormat=='ASCII':
                return 'ascii'
            else:
                for (key,value) in self.DATAENCODING_VALUES.items():
                    if encodingEndianness == value[0] and encodingFormat == value[1] and encodingUnsignedness == value[2]:
                        return key
            raise ValueError("Invalid data format.")

        @encoding.setter
        def encoding(self,value):
            if value not in self.DATAENCODING_VALUES.keys():
                raise ValueError("Invalid data format.")

            toSend = self.DATAENCODING_VALUES[value]
            #self.write(':WAVeform:BYTeorder '+ toSend[0])
            self.write(':WAVeform:FORMat '+ toSend[1])
            #self.write(':WAVeform:UNSigned '+ str(toSend[2]))
