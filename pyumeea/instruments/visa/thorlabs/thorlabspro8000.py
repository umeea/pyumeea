#
# This file is part of the PyMeasure package.
#
# Copyright (c) 2013-2020 PyMeasure Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

from pymeasure.instruments import Instrument, discreteTruncate
from pymeasure.instruments.validators import strict_discrete_set, \
    truncated_discrete_set, truncated_range

import numpy as np
import time
import re


class ThorlabsPro800(ThorlabsProMainFrame):
    SLOTS = range(1,2)

class ThorlabsPro8000(ThorlabsProMainframe):
    SLOTS = range(1,9)
    


class ThorlabsProMainframe(Instrument):
    """Represents generic Thorlabs Pro mainframe / modular laser driver"""
    MODULES_TYPES = {(191,0):ThorlabsProModules.LDC,
                     (223,0):ThorlabsProModules.TED,
                     (223,1):ThorlabsProModules.TED_PT,
                     (223,2):ThorlabsProModules.TED_KRYO,
                     (107,0):ThorlabsProModules.PDA_NOUFWD_1CH,
                     (107,1):ThorlabsProModules.PDA_NOUFWD_2CH,
                     (107,2):ThorlabsProModules.PDA_UFWD_1CH,
                     (107,3):ThorlabsProModules.PDA_UFWD_2CH,                     
                    }   
    
    SLOTS = range(1,9)
    LDC_POLARITIES = ['AG', 'CG']
    STATUS = ['ON','OFF']


    def __init__(self, resourceName, name="genericThorlabsProMainframe", autoAddModules=True,**kwargs):
        super(ThorlabsPro8000, self).__init__(
            resourceName,
            name,
            **kwargs
        )
        self.write(':SYST:ANSW VALUE')
        self.modules={}
        if autoAddModules is True:
            self.autoAddModules()

    # Code for general purpose commands (mother board related)
    def addModule(self,slotNum,boardKind):
        self.modules[slotNum] = boardKind(self)
        
    def autoAddModules(self):
        listModules = getModulesInstalled(self).split(',')
        for k in range(0,len(listModules),2):
            main_id = int(listModules[k].strip())
            sub_id  = int(listModules[k+1].strip())
            if main_id != 0:
                self.modules[int(k/2)] = MODULES_TYPES[(main_id,sub_id)](self)                        
        
    def getModulesInstalled(self):
        modulesList = self.ask(":CONFIG:PLUG?")
        
    
    slot = Instrument.control(":SLOT?", ":SLOT %d",
                                "Slot selection. Allowed values are: {}""".format(SLOTS),
                                validator=strict_discrete_set,
                                values=SLOTS,
                                map_values=False,cast=int)

    # Code for LDC-xxxx daughter boards (laser driver)


    # Code for TED-xxxx daughter boards (laser driver)


    
class ThorlabsProModules(object):
    class Group(object):
        def __init__(self, parent,replaceList={}):
            self.parent = parent
            self.replaceList = replaceList
        def pC(self,command): #parsedCommand
            theCommand = command
            for k,v in self.replaceList.items():
                theCommand = theCommand.replace(k,v)
            return theCommand
        def ask(self, command):
            return self.parent.ask(self.pC(command))
        def write(self, command):
            self.parent.write(self.pC(command))
        def values(self, command, **kwargs):
            return self.parent.values(self.pC(command), **kwargs)
        def read(self):
            return self.parent.read()
        def binary_values(self, command, header_bytes=0, dtype=np.float32):
            return self.parent.binary_values(self.pC(command), header_bytes, dtype)
        def check_errors(self):
            return self.parent.check_errors()
 
    class Module(Group):
        def __init__(self, parent,slotNum,replaceList={}):
            super(Group, self).__init__(parent,replaceList)
            self.mySlot = slotNum
        def ensureSlot(self):
            if self.parent.slot != self.mySlot:
                self.parent.slot = self.mySlot                
        def ask(self, command):
            self.ensureSlot()
            return super(Group, self).ask(command)
        def write(self, command):
            self.ensureSlot()
            return super(Group, self).write(command)
        

        
        
    class LDC(Group):
        POLARITIES = {'anodeGround':'AG', 'cathodeGround':'CG'}
        MODES = {'constantCurrent':'CC','constantPower':'CP'}
        STATUS = {'on':'ON','off':'OFF'}
        
        def __init__(self, parent):
            super().__init__(parent)
            
        mode =  Instrument.control(":MODE?", ":MODE %s",
                                    """Set laser diode control mode. Allowed values are: {}""".format(MODES),
                                    validator=strict_discrete_set,
                                    values=MODES,
                                    map_values=True)
            
        laserCurrent = Instrument.control(":ILD:SET?", ":ILD:SET %g",
                                    """Wanted Laser current for constant current mode.""")
        laserCurrentLimit = Instrument.control(":LIMC:SET?", ":LIMC:SET %g",
                                    """Set Software current Limit (value must be lower than hardware current limit).""")
        laserPolarity = Instrument.control(":LDPOL:SET?", ":LDPOL:SET %s",
                                    """Set laser diode polarity. Allowed values are: {}""".format(POLARITIES),
                                    validator=strict_discrete_set,
                                    values=POLARITIES,
                                    map_values=True)
        photodiodePolarity = Instrument.control(":PDPOL:SET?", ":PDPOL:SET %s",
                                    """Set monitor photodiode diode polarity. Allowed values are: {}""".format(POLARITIES),
                                    validator=strict_discrete_set,
                                    values=POLARITIES,
                                    map_values=True)
        calibratePhotodiode = Instrument.control(":CALPD:SET?", ":CALPD:SET %s",
                                    """Set monitor photodiode calibration in A/W.""")
        photodiodeCurrent =  Instrument.control(":IMD:SET?", ":IMD:SET %s",
                                    """Wanted Monitor photodiode current for constant power mode.""")        
        status = Instrument.control(":LASER?", ":LASER %s",
                                    """Set laser diode status. Allowed values are: {}""".format(STATUS),
                                    validator=strict_discrete_set,
                                    values=STATUS,
                                    map_values=True)
        actualPhotodiodeCurent = Instrument.measurement(':IMD:ACT?',
                    """Get Actual photodiode current.""")        
        
        actualLaserCurent = Instrument.measurement(':ILD:ACT?',
                    """Get Actual laser current.""")
        laserHardwareCurrentLimit = Instrument.measurement(':LIMCP:ACT?',
                    """Get Hardware laser current limit.""") 
            
    class TED(Group):
        STATUS = {'on':'ON','off':'OFF'}
        
        def __init__(self, parent):
            super().__init__(parent)
            
        status = Instrument.control(":TEC?", ":TEC %s",
                                    """Set TEC status. Allowed values are: {}""".format(STATUS),
                                    validator=strict_discrete_set,
                                    values=STATUS,
                                    map_values=True)
        setTemperature = Instrument.control(":TEMP:SET?", ":TEMP:SET %g",
                                """Set TEC temperature""")
        
    class TED_PT(TED):
        pass
    class TED_KRYO(TED):
        pass
        
    class ITC(Group):
        pass
    
    class PDA(Group):
        RANGES = {10e-9:1,100e-9:2,1e-6:3,10e-6:4,100e-6:5,1e-3:6,10e-3:7}
        POLARITIES = {'anodeGround':'AG', 'cathodeGround':'CG'}
        
        def __init__(self, parent):
            super().__init__(parent)
            
        calibrate = Instrument.control(":CALPD:SET?", ":CALPD:SET %s",
                                    """Set monitor photodiode calibration in A/W.""")       
        getCurrent = Instrument.measurement(':IPD:ACT?',
                    """Get actual photocurrent.""")         
        getPower = Instrument.measurement(':POPT:ACT?',
                    """Get actual photocurrent.""")            
        polarity = Instrument.control(":PDPOL:SET?", ":PDPOL:SET %s",
                                    """Set photodiode polarity. Allowed values are: {}""".format(POLARITIES),
                                    validator=strict_discrete_set,
                                    values=POLARITIES,
                                    map_values=True)           
    class PDA_2CH(Group):
        PORTS = [1,2]
         def __init__(self, parent):
            super().__init__(parent)

        port = Instrument.control(":PORT?", ":PORT %d",
                                    "Port selection. Allowed values are: {}""".format(PORTS),
                                    validator=strict_discrete_set,
                                    values=PORTS,
                                    map_values=False,cast=int)    

                        
    
    class PDA_NOUFWD_1CH(PDA):
        def __init__(self, parent):
            super().__init__(parent)
    
    class PDA_NOUFWD_2CH(PDA_2CH):
        def __init__(self, parent):
            super().__init__(parent)        
          
    class PDA_UFWD_1CH(PDA):
        def __init__(self, parent):
            super().__init__(parent)                   
        pass
    class PDA_UFWD_2CH(PDA_2CH):
        def __init__(self, parent):
            super().__init__(parent)       
            
    class MLC(Group):
        pass
    
    class OSW(Group):
        pass
    
    class WDM(Group):
        pass

    