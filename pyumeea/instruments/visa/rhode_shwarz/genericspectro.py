#
# This file is part of the PyEEA package.
#
# Copyright (c) 2013-2020 PyEEA Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


from pymeasure.instruments import Instrument
from pymeasure.instruments.validators import strict_discrete_set, \
    truncated_discrete_set, truncated_range,strict_range
import numpy as np
import copy
import time



class genericHMS(Instrument):
    """ Represents a generic Rhode & Schwarz spectro.
    Should be compatible with lots of models, like HMS-X
    """

    def __init__(self, resourceName, name="genericHMS",**kwargs):
        super(genericTDS, self).__init__(
            resourceName,name,timeout=3000,
            **kwargs
        )
        self.amplitude   = genericHMSGroups.Amplitude(self)
        self.bandwidth   = genericHMSGroups.Bandwidth(self)
        self.frequency   = genericHMSGroups.Frequency(self)
        self.sweep       = genericHMSGroups.Sweep(self)
        self.measurement = genericHMSGroups.Measurement(self)
        self.trace       = genericHMSGroups.Trace(self)
        self.trigger     = genericHMSGroups.Trigger(self)

    def acquire(self):
        """acquires then downloads a trace."""
        self.sweep.mode='single'
        self.trigger.now()
        while self.sweep.status() != 'READY':
            time.sleep(0.1)
        return self.trace.download()



class genericHMSGroups(object):

    class Group(object):
        def __init__(self, parent,replaceList={}):
            self.parent = parent
            self.replaceList = replaceList

        def pC(self,command): #parsedCommand
            theCommand = command
            for k,v in self.replaceList.items():
                theCommand = theCommand.replace(k,v)
            return theCommand

        def ask(self, command):
            return self.parent.ask(self.pC(command))
        def write(self, command):
            self.parent.write(self.pC(command))
        def values(self, command, **kwargs):
            return self.parent.values(self.pC(command), **kwargs)
        def read(self):
            return self.parent.read()
        def binary_values(self, command, header_bytes=0, dtype=np.float32):
            return self.parent.binary_values(self.pC(command), header_bytes, dtype)
        def check_errors(self):
            return self.parent.check_errors()

    class Amplitude(Group):
        ATTENUATION_VALUES = {'lowNoise':'LNOI','lowDistortion':'LDIS'}
        UNIT_VALUES = {'dBm':'dBm','dBuV':'dBuV','V':'V','W':'W'}
        RANGE_VALUES = {'linear':'LINear','0.5dB/div':'0.5','1dB/div':'1','2dB/div':'2','5dB/div':'5','10dB/div':'10',}

        def __init__(self, parent):
            replaceList = {'#':'AMPL'}
            super().__init__(parent,replaceList)

        attenuatorMode = Instrument.control("#:ATTenuation?", "#:ATTenuation %s",
                                    """Sets or returns the attenuator mode. Possible values are : {}""".format(ATTENUATION_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=ATTENUATION_VALUES,
                                    map_values=True)

        unit = Instrument.control("#:UNIT?", "#:UNIT %s",
                                    """Sets or returns the Physical Unit. Possible values are : {}""".format(UNIT_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=UNIT_VALUES,
                                    map_values=True)

        referenceLevel = Instrument.control("#:RLEVel?", "#:RLEVel %s",
                                    """Sets or returns the reference level""")

        range = Instrument.control("#:RANGe?", "#:RANGe %s",
                                    """Sets or returns the range per division level. Possible values are : {}""".format(RANGE_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=RANGE_VALUES,
                                    map_values=True)
    class Bandwidth(Group):
        STATUS_VALUES={'off':'OFF','on':'ON'}

        def __init__(self, parent):
            replaceList = {'#':'BANDwidth'}
            super().__init__(parent,replaceList)

        resolution = Instrument.control("#:RBW?", "#:RBW %s",
                                    """Defines or Queries the RBW filter value in Hz. """)

        resolutionAuto = Instrument.control("#:RBW:AUTO?", "#:RBW:AUTO %s",
                                    """Queries, Activates or deactivates the automatic RBW fillter setting. Possible values are : {}""".format(STATUS_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)

        video = Instrument.control("#:VBW?", "#:VBW %s",
                                    """Defines or Queries the VBW filter value in Hz. """)

        videoAuto = Instrument.control("#:VBW:AUTO?", "#:VBW:AUTO %s",
                                    """Queries, Activates or deactivates the automatic VBW fillter setting. Possible values are : {}""".format(STATUS_VALUES.keys()),
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)


    class Frequency(Group):

        def __init__(self, parent):
            replaceList = {'#':'FREQuency'}
            super().__init__(parent,replaceList)

        center = Instrument.control("#:CENTer?", "#:CENTer %s",
                                    """Sets or returns the center frequency.""")

        span = Instrument.control("#:SPAN?", "#:SPAN %s",
                                    """Sets or returns the current span""")

        fullSpan = Instrument.measurement('#:SPAN FULL',
                    """Sets the span to full span.""")

        zeroSpan = Instrument.measurement('#:SPAN ZERO',
                    """Sets the span to zero span.""")

        start = Instrument.control("#:STARt?", "#:STARt %s",
                                    """Sets or returns the current start frequency""")

        stop = Instrument.control("#:STOP?", "#:STOP %s",
                                    """Sets or returns the current stop frequency""")

        step = Instrument.control("#:STEP?", "#:STEP %s",
                                    """Sets or returns the center frequency step""")

    class Sweep(Group):

        def __init__(self, parent):
            replaceList = {'#':'SWEep'}
            super().__init__(parent,replaceList)

        STATUS_VALUES={'off':'OFF','on':'ON'}
        MODE_VALUES = {'continuous':'CONT','single':'SING'}

        sweepTime = Instrument.control("#:TIME?", "#:TIME? %s",
                                    """Sets or returns the current sweep time.""")

        sweepTimeAuto = Instrument.control("#:TIME:AUTO?", "#:TIME:AUTO %s",
                                    """Activates or deactivates the automatic sweep time setting. Possible values are : {}""".format(MODE_VALUES.keys())
                                    validator=strict_discrete_set,
                                    values=MODE_VALUES,
                                    map_values=True)

        def status(self):
            """Sets or queries the sweep time mode. returns RUN if running, READY if last single sweep completed."""
            return self.query('#:STATe?')


    class Measurement(Group):

        def __init__(self, parent):
            replaceList = {'#':'MEASure'}
            super().__init__(parent,replaceList)

        STATUS_VALUES={'off':'OFF','on':'ON'}
        TF_VALUES = {'false':'FALSE','true':'TRUE'}

        trackingGenerator = Instrument.control("#:TGENerator?", "#:TGENerator? %s",
                                    """Sets or queries the Tracking Generator status. Possible values are : {}""".format(STATUS_VALUES.keys())
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)

        preamplifier = Instrument.control("#:PAMP?", "#:PAMP? %s",
                                    """Sets or queries the preamplifier status. Possible values are : {}""".format(STATUS_VALUES.keys())
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)

        def modeCenter(self):
            """Activates the receiver mode with the actual center frequency """
            return self.wrute('#:MEASure CFRX')

        def modeM1(self):
            """Activates the receiver mode with the actual marker M1 setting """
            return self.wrute('#:MEASure M1RX')

        isUncal = Instrument.measurement('#:SPAN ZERO',
                    """Queries whether the current instrument setting displays the “UNCAL” message or not. Possible values are : {}""".format(TF_VALUES.keys()),
                    values=TF_VALUES,
                    map_values=True)

    class Trace(Group):

        def __init__(self, parent):
            replaceList = {'#':'TRACe'}
            super().__init__(parent,replaceList)

        MODE_VALUES={'clear':'CLR','maxHold':'MAX','minHold':'MIN','average':'AVER','average':'HOLD'}
        DETECTOR_VALUES={'auto':'AUTO','sample':'SAMP','max':'MAX','min':'MIN'}

        mode = Instrument.control("#:MODE?", "#:MODE? %s",
                                    """Defines or Queries the trace mode. Possible values are : {}""".format(MODE_VALUES.keys())
                                    validator=strict_discrete_set,
                                    values=MODE_VALUES,
                                    map_values=True)

        detector = Instrument.control("#:DETector?", "#:DETector? %s",
                                    """Defines or Queries the trace detector. Possible values are : {}""".format(STATUS_VALUES.keys())
                                    validator=strict_discrete_set,
                                    values=STATUS_VALUES,
                                    map_values=True)

        def download(self):
            """Downloads trace."""
            self.write(':DATA:FORMat CSV')
            data = self.query(':DATA?')
            #parser =  data.splitlines()
            #freq = np.zeros(len(parser))
            #mag = np.zeros(len(parser))
            #for k,line in enumerate(parser):
            #    [f,m] = np.fromstring(line,sep=',')
            #    freq[k] = f
            #    mag[k] = m
            freq,mag = np.loadtxt(data,unpack=True,separator=',') # see https://matplotlib.org/3.1.1/gallery/specialty_plots/skewt.html#sphx-glr-gallery-specialty-plots-skewt-py
            return (freq,mag)

    class Trigger(Group):

        def __init__(self, parent):
            replaceList = {'#':'TRIGger'}
            super().__init__(parent,replaceList)

        SOURCE_VALUES={'internal':'IMM','external':'EXT','video':'VID'}
        SLOPE_VALUES={'positive':'POS','negative':'NEG'}

        source = Instrument.control("#:SOURce?", "#:SOURce? %s",
                                    """Defines or Queries the trigger source. Possible values are : {}""".format(SOURCE_VALUES.keys())
                                    validator=strict_discrete_set,
                                    values=SOURCE_VALUES,
                                    map_values=True)

        slope = Instrument.control("#:SLOPe?", "#:SLOPe? %s",
                                    """Defines or Queries the trigger slope. Possible values are : {}""".format(SLOPE_VALUES.keys())
                                    validator=strict_discrete_set,
                                    values=SLOPE_VALUES,
                                    map_values=True)

        def now(self):
            self.write("#:SOFTware")
