from Phidget22.Phidget import *
from Phidget22.Devices.Stepper import *
import numpy as np
import time



class FOG_Stepper(Stepper):
    def __init__(self):
        super().__init__()
        self.openWaitForAttachment(2500)
        self.setAcceleration(87543)
        self.setCurrentLimit(0.26)
        self.setCurrentPosition(0)

    def rotate(speed,angle):
        speed=abs(speed)
        if speed>0.6:
            speed = 0.6
            self.valueChanged(speed,mode='speedwasHigher')
        if speed <0.01:
            speed = 0.01
            self.valueChanged(speed,mode='speedwasLower')
        speed = np.around(speed*5000/0.0982)

        angle = abs(angle)
        if angle>90:
            angle=90
            self.valueChanged(angle,mode='anglewasHigher')

        theanglesteps=np.around(80000*angle*100/9000)
        self.valueChanged(theanglesteps,mode='maxSteps')

        finalangle=theanglesteps*90/80000
        if finalangle==0: finalangle=1

        self.setVelocityLimit(speed)

        # forward
        self.setEngaged(1)


        currentStep = self.getCurrentPosition()
        self.valueChanged(currentStep,mode='forwardStarts')
        self.valueChanged(currentStep,mode='stepChanged')

        self.setTargetPosition(theanglesteps)

        while currentStep < theanglesteps:
            currentStep = self.getCurrentPosition()
            self.valueChanged(currentStep,mode='stepChanged')

        self.setEngaged(0)
        self.valueChanged(currentStep,mode='forwardFinished')

        time.sleep(2)

        #backward
        self.setEngaged(1)

        currentStep = self.getCurrentPosition()
        self.valueChanged(currentStep,mode='backwardStarts')
        self.valueChanged(currentStep,mode='stepChanged')

        self.setTargetPosition(0)

        while currentStep > 0:
            currentStep = self.getCurrentPosition()
            self.valueChanged(currentStep,mode='stepChanged')

        self.setEngaged(0)
        self.valueChanged(currentStep,mode='backwardFinished')


    def valueChanged(self,mode):pass
