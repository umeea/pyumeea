import time
from Phidget22.PhidgetException import *
from Phidget22.Phidget import *
from Phidget22.Devices.DigitalOutput import *


class phidgetInterfaceKit(DigitalOutput):
    """ object for phidget interface kit type interfaces.
    Made for and tested with 1017_1B [PhidgetInterfaceKit 0/0/8]
    Example : (2 relays used on a single phidget board here)
    sw1 = phidgetInterfaceKit(channel=1)
    sw2 = phidgetInterfaceKit(channel=2)
    sw1.status=1
    sw2.status=0
    print(sw1.status
    print(sw2.status
    """
    def __init__(self,serial=None,channel=None,sleep=0):
        super().__init__()
        self.sleep = sleep
        if serial is not None:
            self.setDeviceSerialNumber(serial)
        if channel is not None:
            self.setChannel(channel)
        self.openWaitForAttachment(5000)

    @property
    def status(self):
        return self.getState()
    @status.setter
    def status(self,value):
        self.setState(value)
        time.sleep(self.sleep)
