from IPython.core.display import display, HTML,Latex,Markdown
from .utils import encodeFile,buildMarkdownTable,setModulePath,modulePath
import re  # regular expressions
import base64
import os
import markdown2 as md
import ipywidgets as widgets



##### display documentation

def parseMarkdownFile(inFileName,imports):
    def replaceCommand(inBloc):
        returnString =r''
        str = inBloc.group(0)
        match  = re.search('(?<=<!--)(\S*)([\s\S]*)?-->',str)

        if match.group(1)=='import':
            matchImport = re.search('id="([\s\S]*)"',match.group(2))
            returnString= imports[matchImport.group(1)]#eval(evalStr)
        elif match.group(1)=='img':
            def replaceImage(inBloc):
                _, file_extension = os.path.splitext(inBloc.group(1))
                file_extension = file_extension.lower()
                if file_extension =='.svg':
                    replacement = 'src="data:image/svg+xml;base64,'+encodeFile(inBloc.group(1))+'"'
                elif file_extension == '.png':
                    replacement = 'src="data:image/png;base64,'+encodeFile(inBloc.group(1))+'"'
                elif file_extension == '.jpg':
                     replacement = 'src="data:image/jpg;base64,'+encodeFile(inBloc.group(1))+'"'
                return replacement
            returnString = '<img '+re.sub('src="([\s\S]*)"',replaceImage,match.group(2))+'>'
        return returnString

    f=open(inFileName,'rb')
    MD = f.read()
    MD = MD.decode('utf-8')
    MDparsed = re.sub(r'(?=<!--)[\s\S]*?-->',replaceCommand,MD)
    return MDparsed

moduleFullPath= os.path.dirname(os.path.abspath(__file__))


def docBuilder(docFile,imports={}):
    if isinstance(docFile,list) or isinstance(docFile,tuple):
        MDFileName = os.path.join(moduleFullPath,*docFile)
    else:
        MDFileName = os.path.join(moduleFullPath,docFile)

    cssFile = 'jupyterlab_codehilite.css'
    cssFileName = os.path.join(moduleFullPath,'data',cssFile)

    text= parseMarkdownFile(MDFileName,imports=imports )

    cssData = open(cssFileName,'rb').read().decode('utf-8') +\
    """.jp-RenderedHTMLCommon th {
    font-weight: bold;
    text-align: center !important;}
    .codehilite pre {
    background:var(--jp-rendermime-table-row-background);
    margin-bottom: 10px;
    }
    .codehilite code {
        background-color: var(--jp-rendermime-table-row-background);

    }
    """

    htmlHeader = '<style>\n'+cssData+'</style>\n'
    html = htmlHeader + md.markdown(text,extras = ('tables','fenced-code-blocks','code-friendly'))#,'break-on-newline') )

    display(HTML(html))
